<?php
session_start();
?>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="bootstrap-3.2.0-dist\css\bootstrap.css">
    <title>Registration</title>
</head>
<style>
    .login-panel {
        margin-top: 135px;
    }
</style>
<body>

<div class="container"><!-- container class is used to centered  the body of the browser with some decent width-->
    <div class="row"><!-- row class is used for grid system in Bootstrap-->
        <div class="col-md-4 col-md-offset-4"><!--col-md-4 is used to create the no of colums in the grid also use for medimum and large devices-->
            <div class="login-panel panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">User Registration</h3>
                </div>
                <div class="panel-body">
                    <form role="form" method="post" action="registration.php">
                        <fieldset>
                            <div class="form-group">
                            <p>        <?php
                                if(isset($_SESSION["action1"])){
                                    $error = $_SESSION["action1"];
                                    echo "<span>$error</span>";
                                }
                            ?>        </p>
                                <input class="form-control" required pattern="[A-Za-z\s]+" placeholder="Username" title="enter valid name"  name="name" type="text" autofocus>
                            </div>

                            <div class="form-group">
                                <input class="form-control" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="E-mail" title="Enter valid email" name="email" type="email" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"  placeholder="Password" name="pass" type="password" value="">
                            </div>                        
                            <div class="form-group">
                                <input class="form-control" required pattern="[0-9]{10}" title="enter 10 digit " placeholder="Enter Phone No. " name="mob" type="text" value="">
                            </div>

                            <input class="btn btn-lg btn-success btn-block" type="submit" value="register" name="register" >

                        </fieldset>
                    </form>
                    <center><b>Already registered ?</b> <br></b><a href="login.php">Login here</a></center><!--for centered text-->
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>

<?php

include("database/db_conection.php");//make connection here
if(isset($_POST['register']))
{
    $user_name=$_POST['name'];//here getting result from the post array after submitting the form.
    $user_pass=$_POST['pass'];//same
    $user_email=$_POST['email'];//same
    $user_mobile=$_POST['mob'];

//here query check weather if user already registered so can't register again.
    $check_email_query="select * from users WHERE user_email='$user_email'";
    $run_query=mysqli_query($dbcon,$check_email_query);

    if(mysqli_num_rows($run_query)>0)
    {
echo "<script>alert('Email: $user_email is already exists, Please try another one!')</script>";
exit();
    }
//insert the user into the database.
    $insert_user="insert into users (user_name,user_password,user_email,user_mobile) VALUE ('$user_name','$user_pass','$user_email','$user_mobile')";
    if(mysqli_query($dbcon,$insert_user))
            {
        //foruser
        require 'class/class.phpmailer.php';
        $mail = new PHPMailer;

        //Enable SMTP debugging. 
        $mail->SMTPDebug = 0;                               
        //Set PHPMailer to use SMTP.
        $mail->isSMTP();            
        //Set SMTP host name                          
        $mail->Host = "sg2plcpnl0184.prod.sin2.secureserver.net";
        //Set this to true if SMTP host requires authentication to send email
        $mail->SMTPAuth = true;                          
        //Provide username and password     
        $mail->Username = "summet.prajapati@yorcabs.co.in";                 
        $mail->Password = "Password@123";                           
        //If SMTP requires TLS encryption then set it
        $mail->SMTPSecure = "tls";                           
        //Set TCP port to connect to 
        $mail->Port = 587;                                   

        $mail->From = "summet.prajapati@yorcabs.co.in";
        $mail->FromName = "YOR GARAGE";

        $mail->addAddress($user_email,$user_name);
        $mail->addAddress("oyesumit96@gmail.com",$user_name);

        $mail->isHTML(true);

        $mail->Subject = "YOR Garage";
        $mail->Body = "<p>Thanks for your Registration $user_name.<br>YOUR DETAILS:<br> name:$user_name, phone no: $user_mobile </p>";

        //sms        
        $mail->send(); 
        $a='91';
        $num=$a.$user_mobile;
        $t='Thanks%20for%20Registration%20on%20YORGARAGE!!!';
        $url = "http://103.233.76.120/api/mt/SendSMS?user=YORCABS&password=Yorcabs@123&senderid=YORCAB&channel=Trans&DCS=0&flashsms=0&number=$num&text=$t&route=33";

        $ch = curl_init(); 
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_URL,$url);
        $output=curl_exec($ch);
        curl_close($ch);

        echo"<script>window.open('login.php','_self')</script>";
        $_SESSION['action1']="You have Registered successfully..!";
    }

}

?>