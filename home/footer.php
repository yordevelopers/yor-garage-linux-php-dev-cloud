
<!--Main Footer-->
<footer class="main-footer">
	<!--Widgets Section-->
	<div class="widgets-section">
		<div class="auto-container">
			<div class="row clearfix">
			
				<!--Big Column-->
				<div class="big-column col-lg-6 col-md-12 col-sm-12">
					<div class="row clearfix">
						
						<!--Footer Column-->
						<div class="footer-column col-md-6 col-sm-6 col-xs-12">
							<div class="footer-widget logo-widget">
								<div class="logo">
									<a href="index.html"><img src="images/logo.png" alt="" /></a>
								</div>
								<div class="text">This is Photoshop's version  of Lorem]psukroin gravida nibh vel velit auctor aliquet.Aenean sollicitudin, lorem quis bibendum auctor</div>
								<ul class="social-icon-one">
									<li><a href="#" class="fa fa-twitter"></a></li>
									<li><a href="#" class="fa fa-facebook"></a></li>
									<li><a href="#" class="fa fa-google-plus"></a></li>
								</ul>
							</div>
						</div>
						
						<!--Footer Column-->
						<div class="footer-column col-md-6 col-sm-6 col-xs-12">
							<div class="footer-widget recent-post">
								<h2>Recent Posts</h2>
								<div class="post-block">
								<div class="text"><a href="#">We’re Superior In Repair.</a></div>
								<div class="post-date">12.10.2020</div>
							</div>
							<div class="post-block">
								<div class="text"><a href="#">Experts In Diesel Motors.</a></div>
								<div class="post-date">12.10.2020</div>
							</div>
							<div class="post-block">
								<div class="text"><a href="#">We Do It Right, The First Time.</a></div>
								<div class="post-date">12.10.2020</div>
							</div>
							</div>
						</div>
						
					</div>
				</div>
				
				<!--Big Column-->
				<div class="big-column col-lg-6 col-md-12 col-sm-12">
					<div class="row clearfix">
						
						<!--Footer Column-->
						<div class="footer-column col-md-6 col-sm-6 col-xs-12">
							<div class="footer-widget footer-links">
								<h2>Useful Links</h2>
								<ul class="links">
									<li><a href="about.html">About Us</a></li>
								<li><a href="faq.html">FAQ</a></li>
								<!-- <li><a href="#">Our Team</a></li>
								<li><a href="#">Brand</a></li>
								<li><a href="#">Ecosystem</a></li>
								<li><a href="#">Sitemap</a></li> -->
								</ul>
							</div>
						</div>
						
						<!--Footer Column-->
						<div class="footer-column col-md-6 col-sm-6 col-xs-12">
							<div class="footer-widget contact-widget">
								<h2>Get In Contact</h2>
								<ul class="opening-time">
									<li><span>Monday:</span> 9:30 am - 6.00 pm</li>
									<li><span>Tuesday:</span> 9:30 am - 6.00 pm</li>
									<li><span>Wednesday:</span> 9:30 am - 6.00 pm</li>
									<li><span>Thursday:</span> 9:30 am - 6.00 pm</li>
									<li><span>Friday:</span> 9:30 am - 6.00 pm</li>
									<li><span>Saturday:</span> 9:30 am - 6.00 pm</li>
									<li><span>Sunday:</span>Closed</li>
								</ul>
							</div>
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<!--Footer Bottom-->
	<!-- <div class="footer-bottom">
		<div class="auto-container">
			<div class="bottom-inner">
				<div class="row clearfix">
					 -->
					<!--Nav Column-->
					<!-- <div class="nav-column col-lg-6 col-md-12 col-sm-12">
						<ul class="footer-nav">
							<li><a href="#">Home</a></li>
							<li><a href="#">services</a></li>
							<li><a href="#">about us</a></li>
							<li><a href="#">gallery</a></li>
							<li><a href="#">contact</a></li>
						</ul>
					</div> -->
					
					<!--Copyright Column-->
					<!-- <div class="copyright-column col-lg-6 col-md-12 col-sm-12">
						<div class="copyright"><a target="_blank" href="https://www.templateshub.net">Templates Hub</a></div>
					</div> -->
<!-- 						
				</div>
			</div>
		</div>
	</div> -->
	
</footer>

</div>
<!--End pagewrapper-->

<!-- SCROLL BACK BUTTON AND PROGRESS BAR  -->
<a class="topp-link hidee" href="" id="js-top">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
		<span class="screen-reader-text">Back to top</span>
	  </a>
	
	<script>
		// Set a variable for our button element.
		const scrollToTopButton = document.getElementById('js-top');
		const scrollFunc = () => {
		// Get the current scroll value
		let y = window.scrollY;
		
		// If the scroll value is greater than the window height, let's add a class to the scroll-to-top button to show it!
		if (y > 0) {
			scrollToTopButton.className = "topp-link showw";
		} else {
			scrollToTopButton.className = "topp-link hidee";
		}
		};

		window.addEventListener("scroll", scrollFunc);
		const scrollToTop = () => {
		// Let's set a variable for the number of pixels we are from the top of the document.
		const c = document.documentElement.scrollTop || document.body.scrollTop;
		
		// If that number is greater than 0, we'll scroll back to 0, or the top of the document.
		// We'll also animate that scroll with requestAnimationFrame:
		// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame
		if (c > 0) {
			window.requestAnimationFrame(scrollToTop);
			// ScrollTo takes an x and a y coordinate.
			// Increase the '10' value to get a smoother/slower scroll!
			window.scrollTo(0, c - c / 10);
		}
		};
		// When the button is clicked, run our ScrolltoTop function above!
		scrollToTopButton.onclick = function(e) {
		e.preventDefault();
		scrollToTop();
		}
		//END OF SCROLL BACK BUTTON 

		//progressBarScript
		window.onscroll = function() {myFunc()};

		function myFunc() {
		var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
		var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
		var scrolled = (winScroll / height) * 100;
		document.getElementById("myBar").style.width = scrolled + "%";
		}
		</script>
		<!-- END OF SCROLL BACK BUTTON AND PROGRESS BAR  -->

		
	<!-- PRELOADER SCRIPT  -->
	<script>
		$(window).on('load', function() { // makes sure the whole site is loaded 
		$('#status').fadeOut(); // will first fade out the loading animation 
		$('#reloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
		$('body').delay(350).css({'overflow':'visible'});
		})
	</script>
	<!-- END OF PRELOADER SCRIPT  -->

<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/appear.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/jquery-ui.js"></script>
<!--Google Map APi Key-->
<!-- <script src="http://maps.google.com/maps/api/js?key=AIzaSyBg0VrLjLvDLSQdS7hw6OfZJmvHhtEV_sE"></script>
<script src="js/map-script.js"></script> -->
<!--End Google Map APi-->
<script src="js/script.js"></script>

</body>

<!--  51:42  -->
</html>