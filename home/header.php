<!DOCTYPE html>
<html>

<!-- services55:54  -->
<head>
<meta charset="utf-8">
<title> Services</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	<!-- Preloader -->
   <div id="reloader">
  <div id="status">&nbsp;</div>
</div>
 	
    <!-- Main Header-->
    <header class="main-header" >
    	
		<!-- Header Top -->
    	<div class="header-top">
        	<div class="auto-container">
            	<div class="top-outer clearfix">
                    
                    <!--Top Left-->
                    <div class="top-left">
                    	<ul class="links clearfix">
                        	<li><a href="tel:9033508015"><span class="icon fa fa-phone"></span>  Call us 9033508015</a></li>
                            <li><a href="mailto:summet.prajapati@yorcabs.co.in"><span class="icon fa fa-envelope-o"></span>summet.prajapati@yorcabs.co.in</a></li>
                        </ul>
                    </div>
                    
                  <!--Top Right-->
						  <div class="top-right clearfix">
                          <ul class="links">
							  <li> <a href="#"><?php
							  $d= " Welcome! ";
											echo $d." ".$_SESSION['email'];
											?></a></li>

							  <?php if( isset($_SESSION['email']) && !empty($_SESSION['email']) )
                                       {
                                       ?>
                                    <li>  <a href="logout.php">Logout</a></li>
                                             <?php }else{ ?>
                                         <li> <a href="login.php">Login</a></li>
                                            <li> <a href="registration.php">Register</a></li>
                                                <?php } ?>
							  </ul> 
					</div>
                
            </div>
        </div>
        <!-- Header Top End -->
		
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="header-upper-inner clearfix">
                	
                	<div class="pull-left logo-box">
                    	<div class="logo"><a href="index.html"><img src="images/logo.png" alt="" title=""></a></div>
                    </div>
                   	
                   	<div class="nav-outer clearfix">
                    
						<!-- Main Menu -->
						<nav class="main-menu navbar-expand-md">
							<div class="navbar-header">
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>

							<div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
								<ul class="navigation clearfix">
									<li><a href="index.html">Home</a>
										
									</li>
									<li class="dropdown"><a href="about.html">About</a>
										<ul>
											<li><a href="about.html">About Us</a></li>
											<!-- <li><a href="index.html">Team</a></li>
											<li><a href="index.html">Clients</a></li> -->
											<li><a href="faq.html">Faq</a></li>
											<!-- <li><a href="index.html">Price</a></li>
											<li><a href="index.html">What We Do</a></li> -->
										</ul>
									</li>
									<li><a href="services.html">Services</a></li>
									
									
									<li><a href="registration.html">Contact us</a></li>
								</ul>
							</div>
							
						</nav>
						
					</div>
                   <!--Option Box-->
					<div class="btn-box">
						<a href="registration.html" class="theme-btn btn-style-one">Make a Appoinment</a>
					</div>
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<a href="index.html" class="img-responsive"><img src="images/logo.png" alt="" title=""></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                        <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent1">
                            <ul class="navigation clearfix">
							<li><a href="index.html">Home</a>
										
										</li>
										<li class="dropdown"><a href="about.html">About</a>
											<ul>
												<li><a href="about.html">About Us</a></li>
												<!-- <li><a href="index.html">Team</a></li>
												<li><a href="index.html">Clients</a></li> -->
												<li><a href="faq.html">Faq</a></li>
												<!-- <li><a href="index.html">Price</a></li>
												<li><a href="index.html">What We Do</a></li> -->
											</ul>
										</li>
										<li><a href="services.html">Services</a></li>
										
										
										<li><a href="registration.html">Contact us</a></li>
										<li><a href="registration.html">Book Now</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
				</div>
				
                
				</div>
			<div class="progresss-container">
				<div class="progresss-bar" id="myBar"></div>
			  </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->
<!--Page Title-->
<section class="page-title overlay" style="background-image:url(images/background/12.jpg)">
	<div class="auto-container">
		<h1>Yor Garage</h1>
		<div class="text"> We’ll Fix It!</div>
	</div>
</section>
<!--End Page Title-->