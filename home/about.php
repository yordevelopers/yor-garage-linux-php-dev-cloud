<?php
session_start();

if($_SESSION['email'])
{
if((time() - $_SESSION['last']) > 60) // Time in Seconds
 {
 header("location:logout.php");
 }
 else
 {
 $_SESSION['last'] = time();
 }   
}
else {
	header("Location: index.html");//redirect to login page to secure the welcome page without login access.
	
}
?>




<!DOCTYPE html>
<html>

<!-- services55:54  -->
<head>
<meta charset="utf-8">
<title> Services</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
   <div id="reloader">
  <div id="status">&nbsp;</div>
</div>
    <!-- Main Header-->
    <header class="main-header" >
    	
		<!-- Header Top -->
    	<div class="header-top">
        	<div class="auto-container">
            	<div class="top-outer clearfix">
                    
                    <!--Top Left-->
                    <div class="top-left">
                    	<ul class="links clearfix">
                        	<li><a href="tel:9033508015"><span class="icon fa fa-phone"></span>  Call us 09033508015</a></li>
                            <li><a href="mailto:summet.prajapati@yorcabs.co.in"><span class="icon fa fa-envelope-o"></span>summet.prajapati@yorcabs.co.in</a></li>
                        </ul>
                    </div>
                    
                   <!--Top Right-->
						  <div class="top-right clearfix">
                          <ul class="links">
							  <li> <a href="#"><?php
							  $d= " Welcome! ";
											echo $d." ".$_SESSION['email'];
											?></a></li>

							  <?php if( isset($_SESSION['email']) && !empty($_SESSION['email']) )
                                       {
                                       ?>
                                    <li>  <a href="logout.php">Logout</a></li>
                                             <?php }else{ ?>
                                         <li> <a href="login.php">Login</a></li>
                                            <li> <a href="registration.php">Register</a></li>
                                                <?php } ?>
							  </ul> 
					</div>
                    
                </div>
                
            </div>
        </div>
        <!-- Header Top End -->
		
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="header-upper-inner clearfix">
                	
                	<div class="pull-left logo-box">
                    	<div class="logo"><a href="index.php"><img src="images/logo.png" alt="" title=""></a></div>
                    </div>
                   	
                   	<div class="nav-outer clearfix">
                    
						<!-- Main Menu -->
						<nav class="main-menu navbar-expand-md">
							<div class="navbar-header">
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>

							<div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
								<ul class="navigation clearfix">
									<li><a href="index.php">Home</a>
										
									</li>
									<li class="dropdown"><a href="about.php">About</a>
										<ul>
											<li><a href="about.php">About Us</a></li>
											<!-- <li><a href="index.php">Team</a></li>
											<li><a href="index.php">Clients</a></li> -->
											<li><a href="faq.php">Faq</a></li>
											<!-- <li><a href="index.php">Price</a></li>
											<li><a href="index.php">What We Do</a></li> -->
										</ul>
									</li>
									<li><a href="services.php">Services</a></li>
									
									
									<li><a href="contactus.php">Contact us</a></li>
								</ul>
							</div>
							
						</nav>
						
					</div>
                   <!--Option Box-->
					<div class="btn-box">
						<a href="contactus.php" class="theme-btn btn-style-one">Make a Appoinment</a>
					</div>
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<a href="index.php" class="img-responsive"><img src="images/logo.png" alt="" title=""></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                        <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent1">
                            <ul class="navigation clearfix">
							<li><a href="index.php">Home</a>
										
										</li>
										<li class="dropdown"><a href="about.php">About</a>
											<ul>
												<li><a href="about.php">About Us</a></li>
												<!-- <li><a href="index.php">Team</a></li>
												<li><a href="index.php">Clients</a></li> -->
												<li><a href="faq.php">Faq</a></li>
												<!-- <li><a href="index.php">Price</a></li>
												<li><a href="index.php">What We Do</a></li> -->
											</ul>
										</li>
										<li><a href="services.php">Services</a></li>
										
										
										<li><a href="contactus.php">Contact us</a></li>
                                        <li> <a href="#"><?php							  
											echo $_SESSION['email'];
											?></a></li>
										
							  <?php if( isset($_SESSION['email']) && !empty($_SESSION['email']) )
                                       {
                                       ?>
                                    <li>  <a href="logout.php">Logout</a></li>
                                             <?php }else{ ?>
                                         <li> <a href="login.php">Login</a></li>
                                            <li> <a href="registration.php">Register</a></li>
                                                <?php } ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
				</div>
				
                
				</div>
			<div class="progresss-container">
				<div class="progresss-bar" id="myBar"></div>
			  </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->
	
	<!--Page Title-->
    <section class="page-title overlay" style="background-image:url(images/background/11.jpg)">
    	<div class="auto-container">
        	<h1>About Us</h1>
            <div class="text">We’ll Fix It!</div>
        </div>
    </section>
	<!--End Page Title-->

	<!-- Map Section -->
	<section class="map-section">
		<div class="image-layer" style="background-image:url(images/background/1.jpg)"></div>
		<div class="auto-container">
			<h2>Your Automotive Repair Experts.<span> Where to find us?</span></h2>
			<div class="map-box" style="background-color: white;">
				<!--Map Canvas-->
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3780.4641918285693!2d73.79834531430905!3d18.6431545873377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2b932f34eac1d%3A0xaa2c39f80af5631e!2sYOR%20CABS%20-%20YRA%20TECHNOLOGIES%20PVT.%20LTD.!5e0!3m2!1sen!2sin!4v1578306002605!5m2!1sen!2sin" 
    width="1200" height="400" frameborder="3" 
    style="border:3px solid #ffffff" allowfullscreen=""></iframe>
			</div>
		</div>
	</section>
	<!-- End Map Section -->


	<!--Clients Section-->
    <section class="clients-section" style="background-image:url(images/background/1.jpg)">
        <div class="auto-container">
            
            <div class="sponsors-outer">
                <!--Sponsors Carousel-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/2.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/3.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/4.png" alt=""></a></figure></li>
					<li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/5.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/2.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/3.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/4.png" alt=""></a></figure></li>
                </ul>
            </div>
            
        </div>
    </section>
	<!--End Clients Section-->
	
		<!--Main Footer-->
		<footer class="main-footer">
			<!--Widgets Section-->
			<div class="widgets-section">
				<div class="auto-container">
					<div class="row clearfix">
					
						<!--Big Column-->
						<div class="big-column col-lg-6 col-md-12 col-sm-12">
							<div class="row clearfix">
								
								<!--Footer Column-->
								<div class="footer-column col-md-6 col-sm-6 col-xs-12">
									<div class="footer-widget logo-widget">
										<div class="logo">
											<a href="index.php"><img src="images/logo.png" alt="" /></a>
										</div>
										<div class="text">This is Photoshop's version  of Lorem]psukroin gravida nibh vel velit auctor aliquet.Aenean sollicitudin, lorem quis bibendum auctor</div>
										<ul class="social-icon-one">
											<li><a href="#" class="fa fa-twitter"></a></li>
											<li><a href="#" class="fa fa-facebook"></a></li>
											<li><a href="#" class="fa fa-google-plus"></a></li>
										</ul>
									</div>
								</div>
								
								<!--Footer Column-->
								<div class="footer-column col-md-6 col-sm-6 col-xs-12">
									<div class="footer-widget recent-post">
										<h2>Recent Posts</h2>
										<div class="post-block">
										<div class="text"><a href="#">We’re Superior In Repair.</a></div>
										<div class="post-date">12.10.2020</div>
									</div>
									<div class="post-block">
										<div class="text"><a href="#">Experts In Diesel Motors.</a></div>
										<div class="post-date">12.10.2020</div>
									</div>
									<div class="post-block">
										<div class="text"><a href="#">We Do It Right, The First Time.</a></div>
										<div class="post-date">12.10.2020</div>
									</div>
									</div>
								</div>
								
							</div>
						</div>
						
						<!--Big Column-->
						<div class="big-column col-lg-6 col-md-12 col-sm-12">
							<div class="row clearfix">
								
								<!--Footer Column-->
								<div class="footer-column col-md-6 col-sm-6 col-xs-12">
									<div class="footer-widget footer-links">
										<h2>Useful Links</h2>
										<ul class="links">
											<li><a href="about.php">About Us</a></li>
											<li><a href="faq.php">FAQ</a></li>
											<!-- <li><a href="#">Our Team</a></li>
											<li><a href="#">Brand</a></li>
											<li><a href="#">Ecosystem</a></li>
											<li><a href="#">Sitemap</a></li> -->
										</ul>
									</div>
								</div>
								
								<!--Footer Column-->
								<div class="footer-column col-md-6 col-sm-6 col-xs-12">
									<div class="footer-widget contact-widget">
										<h2>Get In Contact</h2>
										<ul class="opening-time">
											<li><span>Monday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Tuesday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Wednesday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Thursday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Friday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Saturday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Sunday:</span>Closed</li>
										</ul>
									</div>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
			<!--Footer Bottom-->
			<!-- <div class="footer-bottom">
				<div class="auto-container">
					<div class="bottom-inner">
						<div class="row clearfix">
							 -->
							<!--Nav Column-->
							<!-- <div class="nav-column col-lg-6 col-md-12 col-sm-12">
								<ul class="footer-nav">
									<li><a href="#">Home</a></li>
									<li><a href="#">services</a></li>
									<li><a href="#">about us</a></li>
									<li><a href="#">gallery</a></li>
									<li><a href="#">contact</a></li>
								</ul>
							</div> -->
							
							<!--Copyright Column-->
							<!-- <div class="copyright-column col-lg-6 col-md-12 col-sm-12">
								<div class="copyright"><a target="_blank" href="https://www.templateshub.net">Templates Hub</a></div>
							</div> -->
	<!-- 						
						</div>
					</div>
				</div>
			</div> -->
			
		</footer>
		
	</div>
	<!--End pagewrapper-->
	
<!-- SCROLL BACK BUTTON AND PROGRESS BAR  -->
<a class="topp-link hidee" href="" id="js-top">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
		<span class="screen-reader-text">Back to top</span>
	  </a>
	
	<script>
		// Set a variable for our button element.
		const scrollToTopButton = document.getElementById('js-top');
		const scrollFunc = () => {
		// Get the current scroll value
		let y = window.scrollY;
		
		// If the scroll value is greater than the window height, let's add a class to the scroll-to-top button to show it!
		if (y > 0) {
			scrollToTopButton.className = "topp-link showw";
		} else {
			scrollToTopButton.className = "topp-link hidee";
		}
		};

		window.addEventListener("scroll", scrollFunc);
		const scrollToTop = () => {
		// Let's set a variable for the number of pixels we are from the top of the document.
		const c = document.documentElement.scrollTop || document.body.scrollTop;
		
		// If that number is greater than 0, we'll scroll back to 0, or the top of the document.
		// We'll also animate that scroll with requestAnimationFrame:
		// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame
		if (c > 0) {
			window.requestAnimationFrame(scrollToTop);
			// ScrollTo takes an x and a y coordinate.
			// Increase the '10' value to get a smoother/slower scroll!
			window.scrollTo(0, c - c / 10);
		}
		};
		// When the button is clicked, run our ScrolltoTop function above!
		scrollToTopButton.onclick = function(e) {
		e.preventDefault();
		scrollToTop();
		}
		//END OF SCROLL BACK BUTTON 

		//progressBarScript
		window.onscroll = function() {myFunc()};

		function myFunc() {
		var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
		var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
		var scrolled = (winScroll / height) * 100;
		document.getElementById("myBar").style.width = scrolled + "%";
		}
		</script>
		<!-- END OF SCROLL BACK BUTTON AND PROGRESS BAR  -->

		
	<!-- PRELOADER SCRIPT  -->
	<script>
		$(window).on('load', function() { // makes sure the whole site is loaded 
		$('#status').fadeOut(); // will first fade out the loading animation 
		$('#reloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
		$('body').delay(350).css({'overflow':'visible'});
		})
	</script>
	<!-- END OF PRELOADER SCRIPT  -->


	
	<script src="js/jquery.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="js/jquery.fancybox.js"></script>
	<script src="js/appear.js"></script>
	<script src="js/owl.js"></script>
	<script src="js/wow.js"></script>
	<script src="js/jquery-ui.js"></script>
	<!--Google Map APi Key-->
	<!-- <script src="http://maps.google.com/maps/api/js?key=AIzaSyBg0VrLjLvDLSQdS7hw6OfZJmvHhtEV_sE"></script>
	<script src="js/map-script.js"></script> -->
	<!--End Google Map APi-->
	<script src="js/script.js"></script>
	
	</body>
	
	<!--  51:42  -->
	</html>