<!DOCTYPE html>
<html>

<!-- services55:54  -->
<head>
<meta charset="utf-8">
<title> Thank You</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	
   <!-- Preloader -->
   <div id="reloader">
  <div id="status">&nbsp;</div>
</div>
 	
    <!-- Main Header-->
    <header class="main-header" >
    	
		<!-- Header Top -->
    	<div class="header-top">
        	<div class="auto-container">
            	<div class="top-outer clearfix">
                    
                    <!--Top Left-->
                    <div class="top-left">
                    	<ul class="links clearfix">
                        	<li><a href="tel:9033508015"><span class="icon fa fa-phone"></span>  Call us 9033508015</a></li>
                            <li><a href="mailto:summet.prajapati@yorcabs.co.in"><span class="icon fa fa-envelope-o"></span>summet.prajapati@yorcabs.co.in</a></li>
                        </ul>
                    </div>
                    
                    <!--Top Right-->
						  <div class="top-right clearfix">
							<ul class="links">
							
									   <li> <a href="login.php">Login</a></li>
										  <li><a href="registration.php">Register</a></li>
											 
							</ul> 
					</div>
                    
                </div>
                
            </div>
        </div>
        <!-- Header Top End -->
		
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="header-upper-inner clearfix">
                	
                	<div class="pull-left logo-box">
                    	<div class="logo"><a href="index.html"><img src="images/logo.png" alt="" title=""></a></div>
                    </div>
                   	
                   	<div class="nav-outer clearfix">
                    
						<!-- Main Menu -->
						<nav class="main-menu navbar-expand-md">
							<div class="navbar-header">
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>

							<div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
								<ul class="navigation clearfix">
									<li><a href="index.html">Home</a>
										
									</li>
									<li class="dropdown"><a href="about.html">About</a>
										<ul>
											<li><a href="about.html">About Us</a></li>
											<!-- <li><a href="index.html">Team</a></li>
											<li><a href="index.html">Clients</a></li> -->
											<li><a href="faq.html">Faq</a></li>
											<!-- <li><a href="index.html">Price</a></li>
											<li><a href="index.html">What We Do</a></li> -->
										</ul>
									</li>
									<li><a href="services.html">Services</a></li>
									
									
									<li><a href="registration.html">Contact us</a></li>
								</ul>
							</div>
							
						</nav>
						
					</div>
                   <!--Option Box-->
					<div class="btn-box">
						<a href="registration.html" class="theme-btn btn-style-one">Make a Appoinment</a>
					</div>
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<a href="index.html" class="img-responsive"><img src="images/logo.png" alt="" title=""></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                        <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent1">
                            <ul class="navigation clearfix">
							<li><a href="index.html">Home</a>
										
										</li>
										<li class="dropdown"><a href="about.php">About</a>
											<ul>
												<li><a href="about.html">About Us</a></li>
												<!-- <li><a href="index.html">Team</a></li>
												<li><a href="index.html">Clients</a></li> -->
												<li><a href="faq.html">Faq</a></li>
												<!-- <li><a href="index.html">Price</a></li>
												<li><a href="index.html">What We Do</a></li> -->
											</ul>
										</li>
										<li><a href="services.html">Services</a></li>
										
										
										<li><a href="registration.html">Contact us</a></li>
										<li><a href="login.php">Login</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
				</div>
				
                
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->
	
	<!--Page Title-->
    <section class="page-title overlay" style="background-image:url(images/background/11.jpg)">
    	<div class="auto-container">
        	<h1>Thank You</h1>
            <div class="text"></div>
        </div>
    </section>
	<!--End Page Title-->
<?php
$con = mysqli_connect('127.0.0.1','root','');
if(!$con)
{
echo 'Not connected to server ';
}
if(!mysqli_select_db($con,'demo'))
{
    echo 'Database not selected ';
}
$cname = $_POST['term'];
$mob = $_POST['mobi'];

$sql= "INSERT INTO booking (cname,mob) VALUES('$cname','$mob')";

if(!mysqli_query($con,$sql))
{
    echo 'Not inserted';
}
else
{

}

//foruser
require 'class/class.phpmailer.php';
$mail = new PHPMailer;

//Enable SMTP debugging. 
$mail->SMTPDebug = 0;                               
//Set PHPMailer to use SMTP.
$mail->isSMTP();            
//Set SMTP host name                          
$mail->Host = "sg2plcpnl0184.prod.sin2.secureserver.net";
//Set this to true if SMTP host requires authentication to send email
$mail->SMTPAuth = true;                          
//Provide username and password     
$mail->Username = "summet.prajapati@yorcabs.co.in";                 
$mail->Password = "Password@123";                           
//If SMTP requires TLS encryption then set it
$mail->SMTPSecure = "tls";                           
//Set TCP port to connect to 
$mail->Port = 587;                                   

$mail->From = "summet.prajapati@yorcabs.co.in";
$mail->FromName = "YOR GARAGE";


$mail->addAddress("oyesumit96@gmail.com",$cname);

$mail->isHTML(true);

$mail->Subject = "CAR SERVICE";
$mail->Body = "<p>CAR Service Registration.<br>CAR DETAILS:<br> name:$cname, phone no: $mob </p>";


$mail->send(); 

//SMS API CALL 
$a='91';
$num=$a.$mob;
$t='Thanks%20for%20Registration%20on%20YORGARAGE!!!';
$url = "http://103.233.76.120/api/mt/SendSMS?user=YORCABS&password=Yorcabs@123&senderid=YORCAB&channel=Trans&DCS=0&flashsms=0&number=$num&text=$t&route=33";

$ch = curl_init(); 
curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch,CURLOPT_URL,$url);
$output=curl_exec($ch);
curl_close($ch);

// //header("refresh:1; url=index.html");

// mysqli_close($con);

// //the subject
// $sub = "YORGarage client  ";
// //the message
// $msg = "Car name:$cname, phone no: $mob  ";
// //recipient email here
// $rec = "sushantnikam4@gmail.com";
// //send email
// mail($rec,$sub,$msg);


?>

    <!--Main Footer-->
		<footer class="main-footer">
			<!--Widgets Section-->
			<div class="widgets-section">
				<div class="auto-container">
					<div class="row clearfix">
					
						<!--Big Column-->
						<div class="big-column col-lg-6 col-md-12 col-sm-12">
							<div class="row clearfix">
								
								<!--Footer Column-->
								<div class="footer-column col-md-6 col-sm-6 col-xs-12">
									<div class="footer-widget logo-widget">
										<div class="logo">
											<a href="index.html"><img src="images/logo.png" alt="" /></a>
										</div>
										<div class="text">This is Photoshop's version  of Lorem]psukroin gravida nibh vel velit auctor aliquet.Aenean sollicitudin, lorem quis bibendum auctor</div>
										<ul class="social-icon-one">
											<li><a href="#" class="fa fa-twitter"></a></li>
											<li><a href="#" class="fa fa-facebook"></a></li>
											<li><a href="#" class="fa fa-google-plus"></a></li>
										</ul>
									</div>
								</div>
								
								<!--Footer Column-->
								<div class="footer-column col-md-6 col-sm-6 col-xs-12">
									<div class="footer-widget recent-post">
										<h2>Recent Posts</h2>
										<div class="post-block">
										<div class="text"><a href="#">We’re Superior In Repair.</a></div>
										<div class="post-date">12.10.2020</div>
									</div>
									<div class="post-block">
										<div class="text"><a href="#">Experts In Diesel Motors.</a></div>
										<div class="post-date">12.10.2020</div>
									</div>
									<div class="post-block">
										<div class="text"><a href="#">We Do It Right, The First Time.</a></div>
										<div class="post-date">12.10.2020</div>
									</div>
									</div>
								</div>
								
							</div>
						</div>
						
						<!--Big Column-->
						<div class="big-column col-lg-6 col-md-12 col-sm-12">
							<div class="row clearfix">
								
								<!--Footer Column-->
								<div class="footer-column col-md-6 col-sm-6 col-xs-12">
									<div class="footer-widget footer-links">
										<h2>Useful Links</h2>
										<ul class="links">
										<li><a href="about.html">About Us</a></li>
										<li><a href="faq.html">FAQ</a></li>
										<!-- <li><a href="#">Our Team</a></li>
										<li><a href="#">Brand</a></li>
										<li><a href="#">Ecosystem</a></li>
										<li><a href="#">Sitemap</a></li> -->
										</ul>
									</div>
								</div>
								
								<!--Footer Column-->
								<div class="footer-column col-md-6 col-sm-6 col-xs-12">
									<div class="footer-widget contact-widget">
										<h2>Get In Contact</h2>
										<ul class="opening-time">
											<li><span>Monday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Tuesday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Wednesday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Thursday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Friday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Saturday:</span> 9:30 am - 6.00 pm</li>
											<li><span>Sunday:</span>Closed</li>
										</ul>
									</div>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
			<!--Footer Bottom-->
			<!-- <div class="footer-bottom">
				<div class="auto-container">
					<div class="bottom-inner">
						<div class="row clearfix">
							 -->
							<!--Nav Column-->
							<!-- <div class="nav-column col-lg-6 col-md-12 col-sm-12">
								<ul class="footer-nav">
									<li><a href="#">Home</a></li>
									<li><a href="#">services</a></li>
									<li><a href="#">about us</a></li>
									<li><a href="#">gallery</a></li>
									<li><a href="#">contact</a></li>
								</ul>
							</div> -->
							
							<!--Copyright Column-->
							<!-- <div class="copyright-column col-lg-6 col-md-12 col-sm-12">
								<div class="copyright"><a target="_blank" href="https://www.templateshub.net">Templates Hub</a></div>
							</div> -->
	<!-- 						
						</div>
					</div>
				</div>
			</div> -->
			
		</footer>
		
	</div>
	<!--End pagewrapper-->
	
<script>
	$(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#reloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(350).css({'overflow':'visible'});
})
	</script>


	
	<script src="js/jquery.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="js/jquery.fancybox.js"></script>
	<script src="js/appear.js"></script>
	<script src="js/owl.js"></script>
	<script src="js/wow.js"></script>
	<script src="js/jquery-ui.js"></script>
	<!--Google Map APi Key-->
	<!-- <script src="http://maps.google.com/maps/api/js?key=AIzaSyBg0VrLjLvDLSQdS7hw6OfZJmvHhtEV_sE"></script>
	<script src="js/map-script.js"></script> -->
	<!--End Google Map APi-->
	<script src="js/script.js"></script>
	
	</body>
	
	<!--  51:42  -->
	</html>

