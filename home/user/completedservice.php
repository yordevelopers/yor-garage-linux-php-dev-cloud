<?php
 session_start();
 if (strlen($_SESSION['email']=="")) {
     header('location:../user/login.php');
     }
 $con = mysqli_connect("localhost", "root", "", "demo");
 $mob=$_SESSION['contact'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta name="description" content="Miminium Admin Template v.1">
	<meta name="author" content="Isna Nur Azis">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>COMPLETEDSERVICE</title>
 
    <!-- start: Css -->
    <link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">

      <!-- plugins -->
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="asset/css/plugins/datatables.bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="asset/css/plugins/animate.min.css"/>
  <link href="asset/css/style.css" rel="stylesheet">
	<!-- end: Css -->

	<link rel="shortcut icon" href="asset/img/logomi.png">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

 <body id="mimin" class="dashboard">
      <!-- start: Header -->
        <nav class="navbar navbar-default header navbar-fixed-top">
          <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
              <div class="opener-left-menu is-open">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
              </div>
              <a href="dashboard.php" class="navbar-brand"> 
                 <b>DASHBOARD</b>
                </a>


              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span><?php echo $_SESSION['name'];?></span></li>
                  <li class="dropdown avatar-dropdown">
                   <img src="asset/img/avatar.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                   <ul class="dropdown-menu user-dropdown">
                     <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
                     
                        <li><a href="logout.php"><span class="fa fa-power-off "></span> Logout</a></li>
                  </ul>
                </li>
                <li >&nbsp;&nbsp;&nbsp;</li>
              </ul>
            </div>
          </div>
        </nav>
      <!-- end: Header -->

      <div class="container-fluid mimin-wrapper">
  
          <!-- start:Left Menu -->
            <div id="left-menu">
              <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="time">
                      <h1 class="animated fadeInLeft">21:00</h1>
                      <p class="animated fadeInRight">Sat,October 1st 2029</p>
                    </li>
                    <li><a href="dashboard.php"><span class="fa-home fa"></span>Dashboard</a></li>
                    <li><a href="book.php"><span class="fa fa-pencil-square"></span>Book New Service</a></li>
                   
                    
                  </ul>
                </div>
            </div>
          <!-- end: Left Menu -->

  		
          <!-- start: content -->
            <div id="content">
                    <div class="panel box-shadow-none content-header">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <br>
                                <p class="animated fadeInDown">
                                <span class="fa-home fa"> <span class="fa-angle-right fa"></span> Completed Services
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading"><h3>Completed Services</h3></div>
                            <div class="panel-body">
                            <div class="responsive-table">
                            <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                <th>Booking ID</th>
                                <th>Name</th>
                                <th>Mobile No.</th>
                                <th>Car Brand</th>
                                <th>Car Model</th>
                                <th>Car Variant</th>
                                <th>Car No.</th>
                                <th>Status</th>
                                <th>Booking Date</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php $ret=mysqli_query($con,"SELECT * FROM bookings WHERE b_mobile ='$mob' and status='complete'");
							  
							  while($row=mysqli_fetch_array($ret))
							  {?>
                              <tr>
                                  <td><?php echo $row['bid'];?></td>
                                  <td><?php echo $row['b_name'];?></td>
                                  <td><?php echo $row['b_mobile'];?></td>
                                  <td><?php echo $row['car_brand'];?></td>
                                  <td><?php echo $row['car_model'];?></td>
                                  <td><?php echo $row['car_variant'];?></td>
                                  <td><?php echo $row['car_no'];?></td>
                                  <td><?php echo $row['status'];?></td>
                                  <td><?php echo $row['b_date'];?></td>
                              </tr>
                              <?php }?>

                              </tbody>
                                </table>
                            </div>
                          </div>
                        </div>
                     </div>
                </div>
      		  </div>
          <!-- end: content -->

    
          
          
      </div>

      <!-- start: Mobile -->
      <div id="mimin-mobile" class="reverse">
        <div class="mimin-mobile-menu-list">
            <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
                <ul class="nav nav-list">
                  <li><a href="dashboard.php"><span class="fa-home fa"></span>Dashboard</a></li>
                  <li><a href="book.php"><span class="fa fa-pencil-square"></span>Book New Service</a></li>
                    
                  </ul>
            </div>
        </div>       
      </div>
      <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
        <span class="fa fa-bars"></span>
      </button>
       <!-- end: Mobile -->

    <!-- start: Javascript -->
    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/jquery.ui.min.js"></script>
    <script src="asset/js/bootstrap.min.js"></script>
   
    
    <!-- plugins -->
    <script src="asset/js/plugins/moment.min.js"></script>
<script src="asset/js/plugins/jquery.datatables.min.js"></script>
<script src="asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="asset/js/plugins/jquery.nicescroll.js"></script>


  <!-- custom -->
<script src="asset/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
  });
</script>
  <!-- end: Javascript -->
  </body>
</html>