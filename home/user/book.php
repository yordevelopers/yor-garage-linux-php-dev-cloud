<?php
session_start();
if (strlen($_SESSION['email']=="")) {
    header('location:../user/login.php');
    }
$con = mysqli_connect("localhost", "root", "", "demo");
$mob=strlen($_SESSION['contact']);
if(isset($_POST['submit'])){
    $cname = $_POST['term'];
    $pieces = explode(" ", $cname);
	$brand=$pieces[0];
	$model=$pieces[1];
	$variant=$pieces[2];
     $name =$_POST['name'];
	$mob = $_POST['cont'];
	$carno=$_POST['cno'];
    
    $sql= "INSERT INTO bookings (b_name,b_mobile,car_brand,car_model,car_variant,car_no) VALUES('$name','$mob','$brand','$model','$variant','$carno')";
    
    if(!mysqli_query($con,$sql))
    {
        echo 'Not inserted';
    }
    else
    {
		$_SESSION["msg"]="SERVICE BOOKED!!!";
    }
    
       

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta name="description" content="Miminium Admin Template v.1">
	<meta name="author" content="Isna Nur Azis">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BOOKSERVICE</title>
 
    <!-- start: Css -->
    <link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">

      <!-- plugins -->
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/font-awesome.min.css"/>
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/simple-line-icons.css"/>
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/animate.min.css"/>
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/fullcalendar.min.css"/>
      <link rel="stylesheet" type="text/css" href="../responsiveform.css">
	<link href="asset/css/style.css" rel="stylesheet">
	<!-- end: Css -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
 
<script>
 $(function() {
    $( "#search" ).autocomplete({
        source: 'search.php'
        
    });
 });
</script>
<style>
.ui-autocomplete {
  width:57%;
    background: #fff;
    border-radius: 0px;
}
.ui-autocomplete.source:hover {
    background: #fff;
    color:white;
}

 .ui-menu .ui-menu-item a{
  color:#000;
    height:5px;
  font-size:10px;
  border-bottom:#000 1px solid;
 }

  </style>
	<link rel="shortcut icon" href="asset/img/logomi.png">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

 <body id="mimin" class="dashboard">
      <!-- start: Header -->
        <nav class="navbar navbar-default header navbar-fixed-top">
          <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
              <div class="opener-left-menu is-open">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
              </div>
                <a href="dashboard.php" class="navbar-brand"> 
                 <b>DASHBOARD</b>
                </a>


              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span><?php echo $_SESSION['name'];?></span></li>
                  <li class="dropdown avatar-dropdown">
                   <img src="asset/img/avatar.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                   <ul class="dropdown-menu user-dropdown">
                     <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
                     
                        <li><a href="logout.php"><span class="fa fa-power-off "></span> Logout</a></li>
                  </ul>
                </li>
                <li >&nbsp;&nbsp;&nbsp;</li>
              </ul>
            </div>
          </div>
        </nav>
      <!-- end: Header -->

      <div class="container-fluid mimin-wrapper">
  
          <!-- start:Left Menu -->
            <div id="left-menu">
              <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="time">
                      <h1 class="animated fadeInLeft">21:00</h1>
                      <p class="animated fadeInRight">Sat,October 1st 2029</p>
                    </li>
                    <li><a href="dashboard.php"><span class="fa-home fa"></span>Dashboard</a></li>
                    <li><a href="book.php"><span class="fa fa-pencil-square"></span>Book New Service</a></li>
                    <li><a href="bookings.php"><span class="fa fa-pencil-square"></span>My Bookings</a></li>
                    
                  </ul>
                </div>
            </div>
          <!-- end: Left Menu -->

  		
          <!-- start: content -->
            <div id="content">
                 <div class="col-md-12" style="padding:20px;">
                    <div class="col-md-12 padding-0">
                    <div class="col-md-12 panel">
                    <div class="col-md-12 panel-heading">
                      <h4>Enter Booking Details</h4>
                    </div>
                    
                    <div class="col-md-12 panel-body" style="padding-bottom:30px;">
                      <div id="envelope">
		<form action="book.php" method="post">
    <div class="centered align-content-center"> <h2  style="color:#000;"> <?php
                    if(isset($_SESSION["msg"])){
                        $error = $_SESSION["msg"];
                        echo "<span>$error</span>";
                    }
                ?>        </h2></div>
		<label>Name*</label>
		<input name="name" required placeholder="yourname" type="text" width="100px;" pattern="[A-Za-z\s]+" title="Enter Valid name">
		<label>Contact Number*</label>
		<input name="cont" required placeholder="123456789" type="text" pattern="[0-9]{10}" title="enter 10 digit only">		
		<label>Car Name*</label>
		<input  id="search" class='ui-autocomplete' autocomplete="off" title="enter brandname<space>modelname<space>variant eg: hyndai santro petrol" style="text-transform:uppercase" pattern="^\w+\s\w+\s\w+" type="text" name="term"placeholder="Enter Car Name" required >
        <label>Car No.*</label>
		<input name="cno" required placeholder="eg 'MH12BA1120'" type="text" style="text-transform:uppercase" width="100px;" pattern="[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}" title="Enter Valid car number">
		<input id="submit" type="submit" value="submit" name="submit">
		</form>
		</div>

                        <!-- <form class="cmxform" id="signupForm" method="post" action="book.php">
                       
                          <div class="col-md-6">
                            <div class="form-group form-animate-text" style="margin-top:40px !important;">
                              <input type="text" class="form-text" title="Enter Valid name" id="validate_firstname" pattern="[A-Za-z\s]+" name="name" required>
                              <span class="bar"></span>
                              <label>Name*</label>
                            </div>

                            <div class="form-group form-animate-text" style="margin-top:40px !important;">
                              <input type="text" class="form-text" title="Enter only 10 digits " id="validate_lastname" pattern="[0-9]{10}" name="cont" required>
                              <span class="bar"></span>
                              <label>Mobile No.*</label>
                            </div>

                            <div class="form-group form-animate-text" style="margin-top:40px !important;">
                            <input  id="search"  class="form-text" pattern="^\w+\s\w+\s\w+" title=" enter brandname<space>modelname<space>variant EG: hyndai santro petrol" type="text" name="term"required >
                              <span class="bar"></span>
                              <label>Car Name*</label>
                            </div>

                            <div class="form-group form-animate-text" style="margin-top:40px !important;">
                              <input type="text" class="form-text" title="Enter proper Car No. "  pattern="[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}" id="validate_username" name="cno" required>
                              <span class="bar"></span>
                              <label>Car No.*</label>
                            </div>
                          </div>

                                        
                          <div class="col-md-12">
                              <input class="submit btn btn-danger" name="submit" type="submit" value="Submit">
                        </div>
                      </form> -->

                    </div>
                
                </div>

                      <div>
                </div>
      		  </div>
          <!-- end: content -->

    
          
          
      </div>

      <!-- start: Mobile -->
      <div id="mimin-mobile" class="reverse">
        <div class="mimin-mobile-menu-list">
            <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
                <ul class="nav nav-list">
                  <li><a href="dashboard.php"><span class="fa-home fa"></span>Dashboard</a></li>
                  <li><a href="book.php"><span class="fa fa-pencil-square"></span>Book New Service</a></li>
                    <li><a href="bookings.php"><span class="fa fa-pencil-square"></span>My Bookings</a></li>
                  </ul>
            </div>
        </div>       
      </div>
      <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
        <span class="fa fa-bars"></span>
      </button>
       <!-- end: Mobile -->

    <!-- start: Javascript -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>	
    <script src="asset/js/bootstrap.min.js"></script>
   
    
    <!-- plugins -->
    <script src="asset/js/plugins/moment.min.js"></script>
    <script src="asset/js/plugins/fullcalendar.min.js"></script>
    <script src="asset/js/plugins/jquery.nicescroll.js"></script>
    <script src="asset/js/plugins/jquery.vmap.min.js"></script>
    <script src="asset/js/plugins/maps/jquery.vmap.world.js"></script>
    <script src="asset/js/plugins/jquery.vmap.sampledata.js"></script>
    <script src="asset/js/plugins/chart.min.js"></script>


    <!-- custom -->
     <script src="asset/js/main.js"></script>
  <!-- end: Javascript -->
  </body>
</html>
<?php
    unset($_SESSION["msg"]);
?>