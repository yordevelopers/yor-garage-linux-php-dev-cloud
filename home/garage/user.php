<?php
session_start();
$bid=intval($_GET['bid']);
$_SESSION['usr_id']=$bid;
//$sid=intval($_GET['sid']);
$con = mysqli_connect("localhost", "root", "", "demo");
$query = "SELECT * FROM `bookings` WHERE bid='".$bid."'";
$rs = mysqli_query($con,$query); $get = mysqli_fetch_assoc($rs);
$q = "SELECT * FROM `service` WHERE u_id='".$bid."'";
$r = mysqli_query($con,$q); $g = mysqli_fetch_assoc($r);


?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin | Dashboard</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
	$('#delete').click(function(){
		if(confirm("ARE YOU SURE WANT SELECTED SERVICES?"))
		{
			var id = [];
			$(':checkbox:checked').each(function(i){
				id[i] = $(this).val();
			});
			if(id.length === 0)
			{
				alert("Please Select Checkbox");
			}
			else
			{
				$.ajax({
					url:"delete.php",
					method: "POST",
					data:{id:id},
					success:function()
					{
                        var x = document.getElementById("hide1");
                        x.style.display = "none";
                        var y = document.getElementById("hide2");
                        y.style.display = "none";
                        document.getElementById("but").style.visibility = "hidden";
                        document.getElementById("headd").style.visibility = "hidden";
                        document.getElementById("res").innerHTML = "Thanks You.";
					}
				});
			}
		}
	});
});
     $(function(){
          $('select.styled').customSelect();
      });
</script>
  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original="Toggle Navigation"></div>
              </div>
            <a href="#" class="logo"><b>Dashboard</b></a>

            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                <li class="dropdown">
                    <a class="dropdown-toggle logout" data-toggle="dropdown" role="button" aria-expanded="false" href="#"> user</a>
            <ul class="dropdown-menu" role="menu">
            <li><a class="fa fa-power-off" href="logout.php"> logout</a></li>
            </ul>
            </li>
        </ul>
            </div>
        </header>

      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">
                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>
              </ul>
          </div>
      </aside>
      <section id="main-content">
          <section class="wrapper">
          	<br>
                <div class="content-panel col-xs-12   ">
                <h3 id="headd" ><i class="fa fa-angle-right"></i> JOB CARD <hr></h3>
                <p style="color:#F00"> <?php
                    if(isset($_SESSION["msg"])){
                        $error = $_SESSION["msg"];
                        echo "<span>$error</span>";
                    }
                ?>        </p>
                <h1 id="res"></h1>
             <div>
                <form method="POST" action="userfile.php">
                 <div id="hide1">
                   <table id="tab1" class="table table-bordered ">
                    <tr>
                     <td>Car No:</td><td><?php echo $get["car_no"];?></td><td>Car :</td><td><?php echo $get["car_brand"];echo " ";echo $get['car_model'];echo " ";echo$get['car_variant'];?></td></tr>
                    <tr><td>Mobile No:</td><td><?php echo $get["b_mobile"];?></td><td>Delivery Date:</td><td><?php echo $g['delivery_date'] ?></td></tr>
                    <tr><td>Service Date Alloted:</td><td><?php echo $g['service_date'] ?></td><td>Service Time Alloted:</td><td><?php echo $g['service_time'];?></td></tr>
                   </table>
                </div>
                <br>
                <div id="hide2" class="form-group options">
                <table id="tab2" class="table table-bordered centered ">
                <thead><tr><th>SELECT</th><th>SERVICE TYPE</th><th>DESCRIPTION</th><th>PROBLEM</th></tr></thead>
                    <tbody>
                    <?php $t=mysqli_query($con,"SELECT * FROM `servicetypes` WHERE u_id='".$bid."'");
							  while($row=mysqli_fetch_array($t))
							  {?>
                    <tr id="<?php echo $row['s_id'];?>"><td><input type="checkbox" name="id" value="<?php echo $row["s_id"]; ?>"></td><td><?php echo $row['servicename'];?></td><td><?php echo $row['description'];?></td><td><?php echo $row['problem'];?></td></tr>
                    <?php  }?>
                    </tbody>
                </table>
                </div>
                <p id="but" align="center">
                <button type="button" name="delete" id="delete">DONE</button>
                </p>
                
            </form>
            </div>
			</div>
        </section>
        <footer class="panel-footer">
    <div class="pull-right hidden-xs">
        <label>Design By- </label> <a href="#" target="_blank">SP</a>
    </div>
    <strong>Copyright &copy; 2020 <a href="#">YOR Garage</a>.</strong> All rights reserved.
</footer>
      </section>

      </section>
      
     
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/common-scripts.js"></script>
    
</body>
</html>
<script>

</script>
<?php
    unset($_SESSION["msg"]);
?>