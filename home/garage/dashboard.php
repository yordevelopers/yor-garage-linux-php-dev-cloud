<?php
session_start();
if (strlen($_SESSION['email']=="")) {
    header('location:mechaniclogin.php');
    }
$con = mysqli_connect("localhost", "root", "", "demo");


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin | Dashboard</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    <script type="text/javascript">
   $(document).ready(function(){
    var timer = setInterval(function() {
         $.ajax({
        url: 'ajax.php',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
          if (response!==""){
            var result = $.trim(response);
            var len = response.length;
            for(var i=0; i<len; i++){
                var row1=response[i].r1;
              var row2=response[i].r2;
              var row3=response[i].r3;
              var row4=response[i].r4;
              var row5=response[i].r5;
              document.getElementById("h1").innerHTML = row1;
                document.getElementById("h2").innerHTML = row2;
                document.getElementById("h3").innerHTML = row3;
                document.getElementById("h4").innerHTML = row4;
                document.getElementById("h5").innerHTML = row5;
            }
            }
        }
     });
    }, 5000);
});</script>
    <style>

    </style>

  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original="Toggle Navigation"></div>
              </div>
            <a href="#" class="logo"><b><?php echo "welcome ".$_SESSION['name'];?></b></a>

            <div class="top-menu">
            <ul class="nav pull-right top-menu">
            <li><a class="logout fa fa-power-off" href="mechaniclogout.php"></a></li>
</ul>
            </div>
        </header>

      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">
                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>All Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="newbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>New Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="confirmbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>Confirm Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="checkinbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>CheckedIn Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="checkoutbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>CheckedOut Bookings</span>
                      </a>
                  </li>

              </ul>
          </div>
      </aside>
      <section id="main-content">
      <section class="wrapper">

        <br>
        <div class="content-panel col-md-12   ">
        <h3 ><i class="fa fa-angle-right"></i> Dashboard <hr></h3>
        <div class="row">
                        <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-cyan text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase ">ALL Bookings</div>
                                                
                                                <div class="h1 "><h1 id="h1">...
                                                </h1></div>
                                            <div class=" text-uppercase links"><a href="bookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-cyan text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase ">New Bookings</div>
                                                
                                                <div class="h1 "><h1 id="h2">...
                                                </h1></div>
                                            <div class=" text-uppercase links"><a href="newbookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-cyan text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase ">Confirm Bookings</div>
                                                
                                                <div class="h1 "><h1 id="h3">...
                                                </h1></div>
                                            <div class=" text-uppercase links"><a href="confirmbookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-cyan text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase ">CheckedIn Bookings</div>
                                                
                                                <div class="h1 "><h1 id="h4">...
                                                </h1></div>
                                            <div class=" text-uppercase links"><a href="checkinbookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-cyan text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase ">CheckedOut Bookings</div>
                                                
                                                <div class="h1 "><h1 id="h5">...
                                                </h1></div>
                                            <div class=" text-uppercase links"><a href="checkoutbookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

        </div>
        </div>
        </section>
          
        <footer class="panel-footer">
    <div class="pull-right hidden-xs">
        <label>Design By- </label> <a href="#" target="_blank">Summet Prajapati</a>
    </div>
    <strong>Copyright &copy; 2020 <a href="#">YOR Garage</a>.</strong> All rights reserved.
</footer>
      </section>

	  </section>
      <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
    <script>
     $(function(){
          $('select.styled').customSelect();
      });
</script>
</body>
</html>
<?php
    unset($_SESSION["msg"]);
?>