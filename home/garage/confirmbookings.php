<?php
session_start();
if (strlen($_SESSION['email']=="")) {
    header('location:mechaniclogin.php');
    }
$con = mysqli_connect("localhost", "root", "", "demo");
$name=$_SESSION['gname'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
   
    <title>Admin | Manage GARAGE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    <script type="text/javascript">
            $(document).on('click', '.update', function(){
                    var id = this.id;
                            $.ajax({
                                url:"update.php",
                                method: "POST",
                                data:{tid:id},
                                success:function()
                                {
                                    $('tr#'+id+'').css('background-color', '#ccc');
                                    $('tr#'+id+'').fadeOut('slow');
                                }
                            });
            });
            </script>
           <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
           <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
           <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />  

    
  </head>
  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <a href="#" class="logo"><b><?php echo "welcome ".$_SESSION['name'];?></b></a>

            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
            <li><a class="logout fa fa-power-off" href="mechaniclogout.php"></a></li>
</ul>
            </div>
        </header>
      <aside>
          <div id="sidebar"  class="nav-collapse ">
          <ul class="sidebar-menu" id="nav-accordion">
                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>All Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="newbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>New Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="confirmbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>Confirm Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="checkinbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>CheckedIn Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="checkoutbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>CheckedOut Bookings</span>
                      </a>
                  </li>

              </ul>
          </div>
      </aside>
      <section id="main-content">
          <section class="wrapper">
          <br>
              <div class="content-panel col-md-12 ">
          	<h3><i class="fa fa-angle-right"></i> Confirmed Bookings</h3>
              <br>
				<div class="row">



                  <div class="col-md-12">
                      <div class="table-responsive">
                      <table id="employee_data" class="table table-striped table-bordered centered">

                              <thead>
                              <tr>
                                  <th>Booking id</th>
                                  <th>Name</th>
                                  <th>Contact no.</th>
                                  <th>Brand</th>
                                  <th>Model</th>
                                  <th>Variant</th>
                                  <th>Car no.</th>
                                  <th>Status</th>
                                <th>Booking Date</th>
                                  <th>CheckIn</th>
                                  

                              </tr>
                              </thead>
                              <tbody>
                                  
                              <?php 
                              $ret=mysqli_query($con,"select * from bookings where garage_assign='".$name."'and status='confirm'");
							  while($row=mysqli_fetch_array($ret))
							  {?>
                              <tr id="<?php echo $row["bid"]; ?>">
                                  <td><?php echo $row['bid'];?></td>
                                  <td><?php echo $row['b_name'];?></td>
                                  <td><?php echo $row['b_mobile'];?></td>
                                  <td><?php echo $row['car_brand'];?></td>
                                  <td><?php echo $row['car_model'];?></td>
                                  <td><?php echo $row['car_variant'];?></td>
                                  <td><?php echo $row['car_no'];?></td>
                                  <td><?php echo $row['status'];?></td>
                                  <td><?php echo $row['b_date'];?></td>
                                  <td>  <center> <button type="button" class="update btn-success" id="<?php echo $row["bid"]; ?>"><i class="fa fa-pencil"></i></button></center></td>
                              </tr>
                              <?php  }?>

                              </tbody>
                          </table>
                          <div style="">
                          <a class="btn btn-theme" href = "javascript:history.back()" ><i class="fa fa-backward" ></i> Back</a>
                        </div>
                      </div>
                  </div>
              </div>
                              </div>
		</section>
      </section>
	  </section>
    
</body>
  </html>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 $(function(){
          $('select.styled').customSelect();
      });

 </script>

