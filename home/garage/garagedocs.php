<?php 
session_start();
$nam=$_SESSION['email'];
?>
<!DOCTYPE html>
<html>
<head>
<title>GARAGE Documents</title>
<!-------Including jQuery from Google ------>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script src="script.js"></script>
<!------- Including CSS File ------>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="style2.css">
</head>
<body>
<div id="maindiv">
<div id="formdiv">

<form enctype='multipart/form-data' action="" method="post">
<div class="inline-block">

<h4>&nbsp;<a class=" float-right btn btn-success" href="logout.php">Logout</a></h4>
<h2>Welcome <?php echo strtoupper($_SESSION['email']);?> </h2>

</div>

 &nbsp;<h2>GARAGE DOCUMENTS</h2>
<hr>
<br>

NOTE: ***PLEASE Upload documents in One By One Order***. Only JPEG,PNG,JPG Type Image Uploaded. Image Size Should Be Less Than 200KB.*
<hr>
<br>

<table> 


<tr>
<td>
GST-Certificate:
</td>
<td>
<div id="filediv"><input name="file1" type="file" id="file"/></div> 
</td> 
<td>
<input type="submit" value="Upload File" name="submit1" id="upload1" class="upload"/>
</td>
<td>
<?php
$conn = mysqli_connect("localhost", "root", "", "demo");

//<------- FOR 1st Submit Button ------>

if (isset($_POST['submit1']))
 {
  // for the database
 
  $profileImageName1 = $_FILES["file1"]["name"];
  // For image upload
  $target_dir = "garagedocs/gst/";
  $target_file = $target_dir . basename($profileImageName1);
  // VALIDATION
  // validate image size. Size is calculated in Bytes
  if($_FILES['file1']['size'] > 200000) {
    $message = "Image size should not be greated than 200Kb";
    echo "<script type='text/javascript'>alert('$message');</script>";
   // $msg_class = "alert-danger";
        }                   
        elseif (empty($error)) 
       {
                    if(move_uploaded_file($_FILES["file1"]["tmp_name"], $target_file)) 
                    {
                    $sql = "UPDATE mechanic SET GST_Certificate ='$profileImageName1' where m_name='".$nam."' ";
                    if(mysqli_query($conn, $sql))
                    {
                        $message = "Image uploaded and saved in the Database";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        
                        // $msg_class = "alert-success";
                        } 
                    else 
                        {
                            $message = "There was an error in the database";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        //$msg_class = "alert-danger";
                        }
                    }
                    else {
                            $message = "Please Select Proper File ";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                            // $msg = "alert-danger";
                        }
             
        
        }
        $q = "SELECT `GST_Certificate` FROM mechanic WHERE m_name='".$nam."'";
        $result = $conn->query($q);
        $row = $result->fetch_assoc();
        $temp2 = $row["GST_Certificate"];
        if($temp2 == "")
        {
            echo "No files";
        }
        else {
            echo $temp2;
        }
        mysqli_close($conn);       

}


?>
</td>
</tr>

<tr>
    <td>
Service Level Agreement</td>
<td>
<div id="filediv"><input name="file2" type="file" id="file"/></div>
</td>
<td>
<!-- <input type="button" id="add_more" class="upload" value="Add More Files"/> -->
<input type="submit" value="Upload File" name="submit2" id="upload2" class="upload"/>
</td>
<td>
<?php
$conn = mysqli_connect("localhost", "root", "", "demo");
//<------- FOR 2nd Submit Button ------>

if (isset($_POST['submit2']))
 {
  // for the database
 
  $profileImageName2 = $_FILES["file2"]["name"];
  // For image upload
  $target_dir = "garagedocs/ServiceLevelAgreement/";
  $target_file = $target_dir . basename($profileImageName2);
  // VALIDATION
  // validate image size. Size is calculated in Bytes
  if($_FILES['file2']['size'] > 200000) {
    $message = "Image size should not be greated than 200Kb";
    echo "<script type='text/javascript'>alert('$message');</script>";
   // $msg_class = "alert-danger";
    
  }
                   
                elseif (empty($error)) 
                {
                    if(move_uploaded_file($_FILES["file2"]["tmp_name"], $target_file)) 
                    {
                    $sql = "UPDATE mechanic SET Service_level_agreement ='$profileImageName2' where m_name='".$nam."' ";
                    if(mysqli_query($conn, $sql))
                    {
                        $message = "Image uploaded and saved in the Database";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        
                        // $msg_class = "alert-success";
                        } else 
                        {
                            $message = "There was an error in the database";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        //$msg_class = "alert-danger";
                        }
                    }
                    else {
                        $message = "Please Select Proper File ";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                            // $msg = "alert-danger";
                        }
             
                }
                $q = "SELECT `Service_level_agreement` FROM mechanic WHERE m_name='".$nam."'";
                $result = $conn->query($q);
                $row = $result->fetch_assoc();
                $temp2 = $row["Service_level_agreement"];
                if($temp2 == "")
                {
                    echo "No files";
                }
                else {
                    echo $temp2;
                }
                mysqli_close($conn);

}


?>
</td>
</tr>

<tr>
    <td>
    Quality Assurance Agreement:</td>
<td>
<div id="filediv"><input name="file3" type="file" id="file"/></div>
</td>
<td>
<!-- <input type="button" id="add_more" class="upload" value="Add More Files"/> -->
<input type="submit" value="Upload File" name="submit3" id="upload3" class="upload"/>
</td>
<td>
<?php
$conn = mysqli_connect("localhost", "root", "", "demo");

//<------- FOR 3rd Submit Button ------>
  
if (isset($_POST['submit3']))
{
 // for the database

 $profileImageName3 = $_FILES["file3"]["name"];
 // For image upload
 $target_dir = "garagedocs/QualityAssuranceAgreement/";
 $target_file = $target_dir . basename($profileImageName3);
 // VALIDATION
 // validate image size. Size is calculated in Bytes
 if($_FILES['file3']['size'] > 200000) {
    $message = "Image size should not be greated than 200Kb";
    echo "<script type='text/javascript'>alert('$message');</script>";
  // $msg_class = "alert-danger";
    }
   
               elseif (empty($error)) 
               {
                   if(move_uploaded_file($_FILES["file3"]["tmp_name"], $target_file)) 
                   {
                   $sql = "UPDATE mechanic SET Quality_Assurance_Agreement ='$profileImageName3' where m_name='".$nam."' ";
                   if(mysqli_query($conn, $sql))
                   {
                    $message = "Image uploaded and saved in the Database";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                    
                    // $msg_class = "alert-success";
                    } else 
                    {
                        $message = "There was an error in the database";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                       //$msg_class = "alert-danger";
                       }
                   }
                    else {
                        $message = "Please Select Proper File ";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                            // $msg = "alert-danger";
                            }
            
                }
                $q = "SELECT `Quality_Assurance_Agreement` FROM mechanic WHERE m_name='".$nam."'";
                $result = $conn->query($q);
                $row = $result->fetch_assoc();
                $temp3 = $row["Quality_Assurance_Agreement"];
                if($temp3 == "")
                {
                    echo "No files";
                }
                else {
                    echo $temp3;
                }
                mysqli_close($conn);

}


?>
</td>
</tr>

<tr>
    <td>
Payment Agreement:</td>
<td>
<div id="filediv"><input name="file4" type="file" id="file"/></div>
</td>
<td>
<!-- <input type="button" id="add_more" class="upload" value="Add More Files"/> -->
<input type="submit" value="Upload File" name="submit4" id="upload4" class="upload"/>
</td>
<td>
<?php
$conn = mysqli_connect("localhost", "root", "", "demo");

//<------- FOR 4th Submit Button ------>
  
if (isset($_POST['submit4']))
{
 // for the database

 $profileImageName4 = $_FILES["file4"]["name"];
 // For image upload
 $target_dir = "garagedocs/PaymentAgreement/";
 $target_file = $target_dir . basename($profileImageName4);
 // VALIDATION
 // validate image size. Size is calculated in Bytes
 if($_FILES['file4']['size'] > 200000) {
    $message = "Image size should not be greated than 200Kb";
    echo "<script type='text/javascript'>alert('$message');</script>";
  // $msg_class = "alert-danger";
   
      }
               elseif (empty($error)) 
               {
                   if(move_uploaded_file($_FILES["file4"]["tmp_name"], $target_file)) 
                   {
                   $sql = "UPDATE mechanic SET Payment_Agreement  ='$profileImageName4' where m_name='".$nam."' ";
                   if(mysqli_query($conn, $sql))
                   {
                    $message = "Image uploaded and saved in the Database";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                    
                    // $msg_class = "alert-success";
                    } else 
                    {
                        $message = "There was an error in the database";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                       //$msg_class = "alert-danger";
                       }
                   }
                    else {
                        $message = "Please Select Proper File ";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                            // $msg = "alert-danger";
                            }
            
                }
                $q = "SELECT `Payment_Agreement` FROM mechanic WHERE m_name='".$nam."'";
                $result = $conn->query($q);
                $row = $result->fetch_assoc();
                $temp4 = $row["Payment_Agreement"];
                if($temp4 == "")
                {
                    echo "No files";
                }
                else {
                    echo $temp4;
                }
                mysqli_close($conn);

}


?>
</td>
</tr>

<tr>
    <td>
Owner PAN:</td>
<td>
<div id="filediv"><input name="file5" type="file" id="file"/></div>
</td>
<td>
<!-- <input type="button" id="add_more" class="upload" value="Add More Files"/> -->
<input type="submit" value="Upload File" name="submit5" id="upload5" class="upload"/>
</td>
<td>
<?php
$conn = mysqli_connect("localhost", "root", "", "demo");
//<------- FOR 5th Submit Button ------>
  
if (isset($_POST['submit5']))
{
 // for the database

 $profileImageName5 = $_FILES["file5"]["name"];
 // For image upload
 $target_dir = "garagedocs/PAN/";
 $target_file = $target_dir . basename($profileImageName5);
 // VALIDATION
 // validate image size. Size is calculated in Bytes
 if($_FILES['file5']['size'] > 200000) {
    $message = "Image size should not be greated than 200Kb";
    echo "<script type='text/javascript'>alert('$message');</script>";
  // $msg_class = "alert-danger";
   }
               elseif (empty($error)) 
               {
                   if(move_uploaded_file($_FILES["file5"]["tmp_name"], $target_file)) 
                   {
                   $sql = "UPDATE mechanic SET Owner_PAN ='$profileImageName5'";
                   if(mysqli_query($conn, $sql))
                   {
                    $message = "Image uploaded and saved in the Database";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                    
                    // $msg_class = "alert-success";
                    } else 
                    {
                        $message = "There was an error in the database";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                       //$msg_class = "alert-danger";
                       }
                   }
                    else {
                        $message = "Please Select Proper File ";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                            // $msg = "alert-danger";
                            }
            
                }
                $q = "SELECT `Owner_PAN` FROM mechanic WHERE m_name='".$nam."'";
                $result = $conn->query($q);
                $row = $result->fetch_assoc();
                $temp5 = $row["Owner_PAN"];
                if($temp5 == "")
                {
                    echo "No files";
                }
                else {
                    echo $temp5;
                }
                mysqli_close($conn);

}


?>
</td>
</tr>

<tr>
    <td>
Owner Adhaar:</td>
<td>
<div id="filediv"><input name="file6" type="file" id="file"/></div>
</td>
<td>
<!-- <input type="button" id="add_more" class="upload" value="Add More Files"/> -->
<input type="submit" value="Upload File" name="submit6" id="upload6" class="upload"/>
</td>
<td>
<?php
$conn = mysqli_connect("localhost", "root", "", "demo");
//<------- FOR 6th Submit Button ------>

if (isset($_POST['submit6']))
 {
  // for the database
 
  $profileImageName6 = $_FILES["file6"]["name"];
  // For image upload
  $target_dir = "garagedocs/Adhaar/";
  $target_file = $target_dir . basename($profileImageName6);
  // VALIDATION
  // validate image size. Size is calculated in Bytes
  if($_FILES['file6']['size'] > 200000) {
    $message = "Image size should not be greated than 200Kb";
    echo "<script type='text/javascript'>alert('$message');</script>";
   // $msg_class = "alert-danger";
    
       }
          elseif (empty($error)) 
                {
                    if(move_uploaded_file($_FILES["file6"]["tmp_name"], $target_file)) 
                    {
                    $sql = "UPDATE mechanic SET Owner_Adhaar  ='$profileImageName6' where m_name='".$nam."' ";
                    if(mysqli_query($conn, $sql))
                    {
                        $message = "Image uploaded and saved in the Database";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        
                        // $msg_class = "alert-success";
                        } else 
                        {
                            $message = "There was an error in the database";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        //$msg_class = "alert-danger";
                        }
                    }
                    else {
                        $message = "Please Select Proper File ";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                            // $msg = "alert-danger";
                        }
             
                }
                $q = "SELECT `Owner_Adhaar` FROM mechanic WHERE m_name='".$nam."'";
                $result = $conn->query($q);
                $row = $result->fetch_assoc();
                $temp6 = $row["Owner_Adhaar"];
                if($temp6 == "")
                {
                    echo "No files";
                }
                else {
                    echo $temp6;
                }
                mysqli_close($conn);

}



?>
</td>
</tr>


<tr>
    <td>
Bank Cancelled Cheque:</td>
<td>
<div id="filediv"><input name="file7" type="file" id="file"/></div>
</td>
<td>
<!-- <input type="button" id="add_more" class="upload" value="Add More Files"/> -->
<input type="submit" value="Upload File" name="submit7" id="upload7" class="upload"/>
</td>
<td>
<?php
$conn = mysqli_connect("localhost", "root", "", "demo");
//<------- FOR 7th Submit Button ------>

if (isset($_POST['submit7']))
 {
  // for the database
 
  $profileImageName7 = $_FILES["file7"]["name"];
  // For image upload
  $target_dir = "garagedocs/CancelledCheque/";
  $target_file = $target_dir . basename($profileImageName7);
  // VALIDATION
  // validate image size. Size is calculated in Bytes
  if($_FILES['file7']['size'] > 200000) {
    $message = "Image size should not be greated than 200Kb";
    echo "<script type='text/javascript'>alert('$message');</script>";
   // $msg_class = "alert-danger";
    
       }
            elseif (empty($error)) 
                {
                    if(move_uploaded_file($_FILES["file7"]["tmp_name"], $target_file)) 
                    {
                    $sql = "UPDATE mechanic SET Bank_Cancelled_Cheque  ='$profileImageName7' where m_name='".$nam."' ";
                    if(mysqli_query($conn, $sql))
                    {
                        $message = "Image uploaded and saved in the Database";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        
                        // $msg_class = "alert-success";
                        } else 
                        {
                            $message = "There was an error in the database";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        //$msg_class = "alert-danger";
                        }
                    }
                    else {
                        $message = "Please Select Proper File ";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                            // $msg = "alert-danger";
                        }
             
                }
                $q = "SELECT `Bank_Cancelled_Cheque` FROM mechanic WHERE m_name='".$nam."'";
                $result = $conn->query($q);
                $row = $result->fetch_assoc();
                $temp7 = $row["Bank_Cancelled_Cheque"];
                if($temp7 == "")
                {
                    echo "No files";
                }
                else {
                    echo $temp7;
                }
                mysqli_close($conn);

}


?>
</td>
</tr>


<tr>
    <td>
NDA:</td>
<td>
<div id="filediv"><input name="file8" type="file" id="file"/></div>
</td>
<td>
<!-- <input type="button" id="add_more" class="upload" value="Add More Files"/> -->
<input type="submit" value="Upload File" name="submit8" id="upload8" class="upload"/>
</td>
<td>
<?php
$conn = mysqli_connect("localhost", "root", "", "demo");

//<------- FOR 8th Submit Button ------>

if (isset($_POST['submit8']))
 {
  // for the database
 
  $profileImageName8 = $_FILES["file8"]["name"];
  // For image upload
  $target_dir = "garagedocs/NDA/";
  $target_file = $target_dir . basename($profileImageName8);
  // VALIDATION
  // validate image size. Size is calculated in Bytes
  if($_FILES['file8']['size'] > 200000) {
    $message = "Image size should not be greated than 200Kb";
    echo "<script type='text/javascript'>alert('$message');</script>";
   // $msg_class = "alert-danger";
    
       }
             elseif (empty($error)) 
                {
                    if(move_uploaded_file($_FILES["file8"]["tmp_name"], $target_file)) 
                    {
                    $sql = "UPDATE mechanic SET Non_disclosure_agreement  ='$profileImageName8' where m_name='".$nam."' ";
                    if(mysqli_query($conn, $sql))
                    {
                        $message = "Image uploaded and saved in the Database";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        
                        // $msg_class = "alert-success";
                        } else 
                        {
                            $message = "There was an error in the database";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                        //$msg_class = "alert-danger";
                        }
                    }
                    else {
                        $message = "Please Select Proper File ";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                            // $msg = "alert-danger";
                        }
             
                }
                $q = "SELECT `Non_disclosure_agreement` FROM mechanic WHERE m_name='".$nam."'";
                $result = $conn->query($q);
                $row = $result->fetch_assoc();
                $temp8 = $row["Non_disclosure_agreement"];
                if($temp8 == "")
                {
                    echo "No files";
                }
                else {
                    echo $temp8;
                }
                mysqli_close($conn);

}


?>
</td>
</tr>

</table>
<br>
<center><input type="submit" value="Done" name="fin"  class="upload"/></center>
<?php 
$con = mysqli_connect("localhost", "root", "", "demo");
if (isset($_POST['fin']))
{
    

    $nam=$_SESSION['email'];
    $q1 = "SELECT `GST_Certificate` FROM mechanic WHERE m_name='".$nam."'";
    $result1 = $conn->query($q1);
    $row = $result1->fetch_assoc();
    $t1 = $row["GST_Certificate"];
    $q2 = "SELECT `Service_level_agreement` FROM mechanic WHERE m_name='".$nam."'";
    $result2 = $conn->query($q2);
    $row = $result2->fetch_assoc();
    $t2 = $row["Service_level_agreement"];
    $q3 = "SELECT `Quality_Assurance_Agreement` FROM mechanic WHERE m_name='".$nam."'";
    $result3 = $conn->query($q3);
    $row = $result3->fetch_assoc();
    $t3 = $row["Quality_Assurance_Agreement"];
    $q4 = "SELECT `Payment_Agreement` FROM mechanic WHERE m_name='".$nam."'";
    $result4 = $conn->query($q4);
    $row = $result4->fetch_assoc();
    $t4 = $row["Payment_Agreement"];
    $q5 = "SELECT `Owner_PAN` FROM mechanic WHERE m_name='".$nam."'";
    $result5 = $conn->query($q5);
    $row = $result5->fetch_assoc();
    $t5 = $row["Owner_PAN"];
    $q6 = "SELECT `Owner_Adhaar` FROM mechanic WHERE m_name='".$nam."'";
    $result6 = $conn->query($q6);
    $row = $result6->fetch_assoc();
    $t6 = $row["Owner_Adhaar"];
    $q7 = "SELECT `Bank_Cancelled_Cheque` FROM mechanic WHERE m_name='".$nam."'";
    $result7 = $conn->query($q7);
    $row = $result7->fetch_assoc();
    $t7 = $row["Bank_Cancelled_Cheque"];
    $q8 = "SELECT `Non_disclosure_agreement` FROM mechanic WHERE m_name='".$nam."'";
    $result8 = $conn->query($q8);
    $row = $result8->fetch_assoc();
    $t8 = $row["Non_disclosure_agreement"];
    


    if($t1 == "")
    {
        $message = "NO gst File";
        echo "<script type='text/javascript'>alert('$message');</script>";
    }
    elseif ($t2 == "") {
        $message = "NO Service File";
        echo "<script type='text/javascript'>alert('$message');</script>";  
    }
    elseif ($t3 == "") {
        $message = "NO Quality File";
        echo "<script type='text/javascript'>alert('$message');</script>";  
    }
    elseif ($t4 == "") {
        $message = "NO payment File";
        echo "<script type='text/javascript'>alert('$message');</script>";  
    }
    elseif ($t5 == "") {
        $message = "NO Pan File";
        echo "<script type='text/javascript'>alert('$message');</script>";  
    }
    elseif ($t6 == "") {
        $message = "NO Aadhar File";
        echo "<script type='text/javascript'>alert('$message');</script>";  
    }
    elseif ($t7 == "") {
        $message = "NO cancelled cheque File";
        echo "<script type='text/javascript'>alert('$message');</script>";  
    }
    elseif ($t8 == "") {
        $message = "NO NDA File";
        echo "<script type='text/javascript'>alert('$message');</script>";  
    }
    else {
        $sql = "UPDATE mechanic SET docs='YES' where m_name='".$nam."'";
    mysqli_query($con,$sql);
    
    header("Location: pending.php");
    }
    mysqli_close($conn);
    

   
    

}?>
</form>

</div>
</div>
</body>
</html>
 