$(document).ready(function(){
    $.ajax({
        url: 'userfile.php',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
            var len = response.length;
            for(var i=0; i<len; i++){
                var Srno = response[i].id;
                var sname = response[i].servicename;
                var des = response[i].description;
                var pro = response[i].problem;

                var tr_str = "<tr>" +
                    "<td>" + (Srno+1) + "</td>" +
                    "<td>" + sname + "</td>" +
                    "<td>" + des + "</td>" +
                    "<td>" + pro + "</td>" +
                    "</tr>";

                $("#userTable tbody").append(tr_str);
            }

        }
    });
});