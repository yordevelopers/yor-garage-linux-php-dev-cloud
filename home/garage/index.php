<?php
session_start();//session starts here

?>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/bootstrap.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

    
        <link href="css/style.css" rel="stylesheet">
    <title>Login</title>
</head>
<style>

</style>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Login</h3>
                </div>
                <div class="panel-body">
                    <form role="form" method="post" action="proccess.php">
                        <fieldset>

                            <p>        <?php
                                if(isset($_SESSION["action1"])){
                                    $error = $_SESSION["action1"];
                                    echo "<span>$error</span>";
                                }
                            ?>        </p>

                            <div class="form-group"  >
                                &nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="button" data-toggle="modal" data-target="#loginModel">Mechanic</button> &nbsp;OR&nbsp;  
            <button type="button" class="button" data-toggle="modal" data-target="#login">Supplier</button>
                            </div>
                            

                              
                            <!-- Change this to a button or input when using this as a form -->
                          <!--  <a href="index.php" class="btn btn-lg btn-success btn-block">Login</a> -->
                        </fieldset>
                    </form>
					 <center><b>Don't have Account ?</b> <br></b> <button type="button" class="button" data-toggle="modal" data-target="#register">Register</button></center>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" id="loginModel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">login Mechanic</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="mproc.php" autocomplete="off">
                <div class="form-group">
                    <input type="text" required name="mmail" class="form-control" placeholder="Enter Username OR Email">
                    

                </div>
                
                <div class="form-group">
                    <input type="password" required name="mpass" class="form-control" placeholder="Enter Password">                            
                </div>
                 <div class="modal-footer">
                    <button type="submit" class="button">Sign in </button>
                    <!-- <button type="button" class="button"data-toggle="modal" data-target="#register">Sign Up </button> -->

                 </div> 
                 </form>  
            </div>
        </div>
    </div>
</div>
<div class="modal fade" role="dialog" id="login">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Login Supplier</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="sproc.php" autocomplete="off">
                
                <div class="form-group" required id="select">
                    <select name="type" id="type" class="custom-select sources" placeholder="Source" required>
                        <option value="select">- Select USERTYPE -</option>
                        <option value="retailer">Retailer</option>
                        <option value="distributer">Distributer</option>                            
                    </select>  
                </div>           
                <div class="form-group">
                    <input type="text" required name="smail" class="form-control" placeholder="Enter Username OR Email">
                    

                </div>
                
                <div class="form-group">
                    <input type="password" required name="spass" class="form-control" placeholder="Enter Password">                            
                </div>
                 <div class="modal-footer">
                    <button type="submit" class="button">Sign in </button>
                    <!-- <button type="button" class="button"data-toggle="modal" data-target="#register">Sign Up </button> -->

                 </div> 
                 </form>  
            </div>
        </div>
    </div>
</div>
<div class="modal fade" role="dialog" id="register">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Register</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="register.php" autocomplete="off">
                <div class="form-group">
                    <select name="type" id="type" class="custom-select sources" placeholder="Source Type" required>
                        <option value="select">- Select USERTYPE -</option>
                        <option value="supplier">Supplier</option>
                        <option value="mechanic">Mechanic</option>                            
                    </select>  
                </div> 
                <div style='display:none;' class="form-group" id="hideme">
                    <select name="type2" id="type2" class="custom-select sources" placeholder="Source Type2" required>
                        <option value="select2">- Select USERTYPE -</option>
                        <option value="retailer">Retailer</option>
                        <option value="distributer">Distributer</option>                            
                    </select>  
                </div>                            
                <div class="form-group">
                    <input type="text" pattern="[A-Za-z\s]+" title=" enter valid Name" name="name" required class="form-control" placeholder="Enter Name">
                </div>
                <div class="form-group">
                    <input type="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" name="password" required class="form-control" placeholder="Enter Password">                            
                </div>
                <div class="form-group">
                    <input type="text" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="E-mail" title="Enter valid email" name="email" required class="form-control" placeholder="Enter Email">
                </div>  
                <div class="form-group">
                    <input type="number" name="number" required pattern="[0-9]{10}" title="enter 10 digit " class="form-control" placeholder="Enter Contact Number">
                </div>                         
                
                    <button type="submit" name="reg" class="button">register</button>

                 </div>  
                </form> 
            </div>
        </div>
    </div>
</div>


</body>
  <script>
      $(document).ready(function(){
    $('#type').on('change', function() {
      if ( this.value == 'supplier')
      {
        $("#hideme").show();
      }
      else
      {
        $("#hideme").hide();
      }
    });
});
      </script>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
</html>
 
<?php
    unset($_SESSION["action1"]);
?>