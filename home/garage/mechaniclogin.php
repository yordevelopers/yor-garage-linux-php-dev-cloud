<?php
session_start();//session starts here
$error = "username/password incorrect or garage not registered";

$con = mysqli_connect('127.0.0.1','root','');
if(!$con)
{
echo 'Not connected to server ';
}
if(!mysqli_select_db($con,'demo'))
{
    echo 'Database not selected ';
}
if(isset($_POST['login']))
{
        $user_email=$_POST['mmail'];
        $user_pass=$_POST['mpass'];
        $_SESSION['last'] = time();


        $sql="select * from mechanic WHERE m_email='$user_email' and m_password='$user_pass' ";
        $result=mysqli_query($con,$sql);


        $count=mysqli_num_rows($result);

     if($count===1)
        {
            // Register $myusername, $mypassword and redirect to file "home.php"
            
            while($row = mysqli_fetch_array($result))
            {
                
                if($row['verified']=='YES')
                {
                    $i=$row['id'];
                    $_SESSION['id']=$i;
                    $gname=$row['g_name'];
                    $_SESSION['gname']=$gname;
                    $user_name=$row['m_name'];
                    $_SESSION['name']=$user_name;
                    $_SESSION['email']=$user_email;//here session is used and value of $user_email store in $_SESSION.
                    echo "<script>window.open('dashboard.php','_self')</script>";
                    
                }
                elseif ($row['verified']=='NO')
                {
                    $user_name=$row['m_name'];
                    $_SESSION['name']=$user_name;
                    echo "<script>window.open('pending.php','_self')</script>";
                }
                else {
                    $_SESSION['action1']= $error;
                    header("location:mechaniclogin.php");
                    exit();
                    
                }
            }
        }
    else 
    {
     $_SESSION['action1']= $error;
     header("location:mechaniclogin.php");
     exit();
    }
}

//mysql_close();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title> Login</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
  </head>

  <body>
	  <div id="login-page">
	  	<div class="container">
      
	  	
		      <form class="form-login" action="mechaniclogin.php" method="post">
            <h2 class="form-login-heading">GARAGE LOGIN</h2>
            <p style="color:#F00; padding-top:20px;" align="center">
            <?php
                    if(isset($_SESSION["action1"])){
                        $error = $_SESSION["action1"];
                        echo "<span>$error</span>";
                    }
                ?>        </p>
           <div class="login-wrap">
           <input type="text" required name="mmail" class="form-control" placeholder="Enter Email">
		            <br>
                    <input type="password" required name="mpass" class="form-control" placeholder="Enter Password"><br >
                    <input class="btn btn-lg btn-theme btn-block" type="submit" value="login" name="login" >
                <br>
                <center><b>Don't have Account ?</b> <br></b><a href="mechanicregistration.php">Register here</a></center>
		        </div>
              </form>	  	
              
	  	
	  	</div>
	  </div>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>
<?php
    unset($_SESSION["action1"]);
?>
