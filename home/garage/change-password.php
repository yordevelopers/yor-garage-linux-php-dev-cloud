<?php
session_start();
error_reporting(0);
$con = mysqli_connect("localhost", "root", "", "demo");
//Checking session is valid or not
if (strlen($_SESSION['id']==0)) {
  header('location:mechaniclogout.php');
  } else{

 // for  password change
if(isset($_POST['Submit']))
{
$oldpassword=($_POST['oldpass']);
$sql=mysqli_query($con,"SELECT m_password FROM mechanic where m_password='$oldpassword'");
$num=mysqli_fetch_array($sql);
if($num>0)
{
$id=$_SESSION['id'];
$newpass=($_POST['newpass']);
 $ret=mysqli_query($con,"update mechanic set m_password='$newpass' where id='$id'");
$_SESSION['msg']="Password Changed Successfully !!";
//header('location:user.php');
}
else
{
$_SESSION['msg']="Old Password not match !!";
}
}
?>
<script language="javascript" type="text/javascript">
function valid()
{
if(document.form1.oldpass.value=="")
{
alert(" Old Password Field Empty !!");
document.form1.oldpass.focus();
return false;
}
else if(document.form1.newpass.value=="")
{
alert(" New Password Field Empty !!");
document.form1.newpass.focus();
return false;
}
else if(document.form1.confirmpassword.value=="")
{
alert(" Re-Type Password Field Empty !!");
document.form1.confirmpassword.focus();
return false;
}
else if(document.form1.newpass.value.length<6)
{
alert(" Password Field length must be atleast of 6 characters !!");
document.form1.newpass.focus();
return false;
}
else if(document.form1.confirmpassword.value.length<6)
{
alert(" Re-Type Password Field less than 6 characters !!");
document.form1.confirmpassword.focus();
return false;
}
else if(document.form1.newpass.value!= document.form1.confirmpassword.value)
{
alert("Password and Re-Type Password Field do not match  !!");
document.form1.newpass.focus();
return false;
}
return true;
}
</script>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin | Change Password</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <a href="#" class="logo"><b><?php echo "welcome ".$_SESSION['name'];?></b></a>
            <div class="nav notify-row" id="top_menu">



                </ul>
            </div>
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                <li><a class="logout fa fa-power-off" href="mechaniclogout.php"></a></li>
            	</ul>
            </div>
        </header>
      <aside>
          <div id="sidebar"  class="nav-collapse ">
          <ul class="sidebar-menu" id="nav-accordion">
                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>All Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="newbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>New Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="confirmbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>Confirm Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="checkinbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>CheckedIn Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="checkoutbookings.php">
                      <i class="fa fa-file-text"></i>
                          <span>CheckedOut Bookings</span>
                      </a>
                  </li>

              </ul>
          </div>
      </aside>
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Change Password </h3>
				<div class="row">



                  <div class="col-md-12">
                      <div class="content-panel ">
                           <form class="form-horizontal style-form" name="form1" method="post" action="" onSubmit="return valid();">
                           <p style="color:#F00"><?php echo $_SESSION['msg'];?><?php echo $_SESSION['msg']="";?></p>
                          <div class="form-group">
                              <label class="col-md-2 control-label " style="padding-left:40px;"><i class="fa fa-lock"></i> Old Password</label>
                              <input type="password" class="form-check-input col-md-3" name="oldpass" value="" >
                         </div>
                              <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label" style="padding-left:40px;"><i class="fa fa-lock"></i> New Password</label>
                                  <input type="password" class="form-check-input col-md-3" name="newpass" value="" >
                          </div>
                             <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label" style="padding-left:40px;"><i class="fa fa-warning"></i> Confirm Password</label>
                                  <input type="password" class="form-check-input col-md-3" name="confirmpassword" value="" >
                          </div>
                          <div style="margin-left:20px;">
                          <input type="submit" name="Submit" value="Update" class="btn btn-theme">
                          <!-- <input type="submit" name="Submit" value="Submit" class="btn btn-theme"> -->
                         <!-- &nbsp;&nbsp; <a class="btn btn-danger" href = "javascript:history.back()" ><i class="fa fa-backward" ></i> Back</a> -->
                        </div>


                       

                          </form>
                      </div>
                  </div>
              </div>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
        
		</section>
        
        <footer class="panel-footer">
    <div class="pull-right hidden-xs">
        <label>Design By- </label> <a href="#" target="_blank">Summet Prajapati</a>
    </div>
    <strong>Copyright &copy; 2020 <a href="#">YOR Garage</a>.</strong> All rights reserved.
</footer>
      </section>
      </section>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
  <script>
      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
<?php } ?>
