<?php
session_start();//session starts here

?>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="bootstrap-3.2.0-dist\css\bootstrap.css">
    <title>Login</title>
</head>
<style>
    .login-panel {
        margin-top: 135px;
    }
</style>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">User Login</h3>
                </div>
                <div class="panel-body">
                    <form role="form" method="post" action="process.php">
                        <fieldset>

                            <p>        <?php
                                if(isset($_SESSION["action1"])){
                                    $error = $_SESSION["action1"];
                                    echo "<span>$error</span>";
                                }
                            ?>        </p>

                            <div class="form-group"  >
                                <input class="form-control" placeholder="Enter Username OR Email" name="email" type="text" required autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="pass" type="password" required value="">
                            </div>


                                <input class="btn btn-lg btn-success btn-block" type="submit" value="login" name="login" >

                            <!-- Change this to a button or input when using this as a form -->
                          <!--  <a href="index.php" class="btn btn-lg btn-success btn-block">Login</a> -->
                        </fieldset>
                    </form>
					 <center><b>Don't have Account ?</b> <br></b><a href="registration.php">Register here</a></center>
                </div>
            </div>
        </div>
    </div>
</div>


</body>

</html>
 
<?php
    unset($_SESSION["action1"]);
?>