<?php
session_start();
include'dbconnection.php';
// checking session is valid for not
if (strlen($_SESSION['id']==0)) {
  header('location:logout.php');
  } else{

// for deleting user
if(isset($_GET['id']))
{
$adminid=$_GET['id'];
$msg=mysqli_query($con,"delete from users where id='$adminid'");
if($msg)
{
echo "<script>alert('Data deleted');</script>";
}
}
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin | Dashboard</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <style>


    </style>

  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original="Toggle Navigation"></div>
              </div>
            <a href="#" class="logo"><b>Admin Dashboard</b></a>

            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout fa fa-power-off" href="logout.php"></a></li>
            	</ul>
            </div>
        </header>

      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">

              	  <p class="centered"><a href="#"><img src="assets/img/yor.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php echo $_SESSION['login'];?></h5>

                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="manage-users.php" >
                          <i class="fa fa-users"></i>
                          <span>Manage Users</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                          <i class="fa fa-file-text"></i>
                          <span>Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="garages.php">
                          <i class="fa fa-gears"></i>
                          <span>Garages</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="suppliers.php">
                          <i class="fa fa-link"></i>
                          <span>Suppliers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="retailer.php">
                          <i class="fa fa-gear"></i>
                          <span>Retailers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="distributer.php">
                          <i class="fa fa-gear"></i>
                          <span>Distributers</span>
                      </a>
                  </li>


              </ul>
          </div>
      </aside>
      <section id="main-content">
          <section class="wrapper">

				<br>
                <div class="content-panel col-md-12   ">
                <h3 ><i class="fa fa-angle-right"></i> Dashboard <hr></h3>
                <div class="row">
                         <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-cyan text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase">Total Bookings</div>
                                            <div class=" h1 "><?php $sql = "select bid from bookings";
                                            $result = mysqli_query($con,$sql) or die(mysqli_error());
                                            echo mysqli_num_rows($result); ?></div>
                                            <div class=" text-uppercase links"><a href="bookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-orange text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase">New Bookings</div>
                                            <div class=" h1 "><?php $sql = "select bid from bookings where garage_assign='' and status='pending'";
                                            $result = mysqli_query($con,$sql) or die(mysqli_error());
                                            echo mysqli_num_rows($result); ?></div>
                                            <div class=" text-uppercase links"><a href="newbookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-cyan text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase">Garage Assigned Bookings</div>
                                            <div class=" h1 "><?php $sql = "select bid from bookings where garage_assign<>''";
                                            $result = mysqli_query($con,$sql) or die(mysqli_error());
                                            echo mysqli_num_rows($result); ?></div>
                                            <div class=" text-uppercase links"><a href="assignedbookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-cyan text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase">Garage Pending Bookings</div>
                                            <div class=" h1 "><?php $sql = "select bid from bookings where garage_assign='assigned' and status='pending'";
                                            $result = mysqli_query($con,$sql) or die(mysqli_error());
                                            echo mysqli_num_rows($result); ?></div>
                                            <div class=" text-uppercase links"><a href="pendingbookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-cyan text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase">Vehicles Checked In </div>
                                            <div class=" h1 "><?php $sql = "select bid from bookings where garage_assign='assigned' and status='checkin'";
                                            $result = mysqli_query($con,$sql) or die(mysqli_error());
                                            echo mysqli_num_rows($result); ?></div>
                                            <div class=" text-uppercase links"><a href="checkinbookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel-body dark-cyan text-light">
                                        <div class=" text-center">
                                        <div class=" text-uppercase">Vehicles Service Completed </div>
                                            <div class=" h1 "><?php $sql = "select bid from bookings where garage_assign='assigned' and status='checkout'";
                                            $result = mysqli_query($con,$sql) or die(mysqli_error());
                                            echo mysqli_num_rows($result); ?></div>
                                            <div class=" text-uppercase links"><a href="checkoutbookings.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-body dark-red text-light">
								<div class=" text-center">
                                <div class=" text-uppercase">Total Garages</div>
									<div class=" h1 "><?php $sql = "select id from mechanic";
                                    $result = mysqli_query($con,$sql) or die(mysqli_error());
                                    echo mysqli_num_rows($result); ?></div>
									<div class=" text-uppercase links"><a href="garages.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
								</div>
							</div>
						</div>
                    </div>
                    <div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-body dark-cyan text-light">
								<div class=" text-center">
                                <div class=" text-uppercase">Total Active Garages</div>
									<div class=" h1 "><?php $sql = "select verified from mechanic where verified='YES'";
                                    $result = mysqli_query($con,$sql) or die(mysqli_error());
                                    echo mysqli_num_rows($result); ?></div>
									<div class=" text-uppercase links"><a href="activegarage.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
								</div>
							</div>
						</div>
                    </div>
                    <div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-body navi-blue text-light">
								<div class=" text-center">
                                <div class=" text-uppercase">Total Pending Garages</div>
									<div class=" h1 "><?php $sql = "select verified from mechanic where verified='NO'";
                                    $result = mysqli_query($con,$sql) or die(mysqli_error());
                                    echo mysqli_num_rows($result); ?></div>
									<div class=" text-uppercase links"><a href="pendinggarage.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
								</div>
							</div>
						</div>
                    </div>
                    <div class="col-md-3 ">
						<div class="panel panel-default">
							<div class="panel-body dark-yellow text-light">
								<div class=" text-center">
                                <div class=" text-uppercase">Total Deleted Garages</div>
									<div class=" h1 "><?php $sql = "select id from deletedmechanic";
                                    $result = mysqli_query($con,$sql) or die(mysqli_error());
                                    echo mysqli_num_rows($result); ?></div>
									<div class=" text-uppercase links"><a href="deletedgarage.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
								</div>
							</div>
						</div>
					</div>
                    <div class="col-md-3  ">
                                <div  class="panel panel-default ">
                                    <div  class="panel-body dark-orange text-light   ">
                                        <div class=" text-center ">
                                        <div class=" text-uppercase ">Total Users  </div>
                                            <div class="h1 "><?php
                                            $sql = "select id from users";
                                            $result = mysqli_query($con,$sql) or die(mysqli_error());
                                            echo mysqli_num_rows($result);
                                            ?></div>
                                            <div class="text-uppercase links"><a class href="manage-users.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-body navi-blue text-light">
								<div class=" text-center">
                                <div class=" text-uppercase">Total Active Users</div>
									<div class=" h1 "><?php $sql = "select access from users where access='1'";
                                    $result = mysqli_query($con,$sql) or die(mysqli_error());
                                    echo mysqli_num_rows($result); ?></div>
									<div class=" text-uppercase links"><a href="activeuser.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="panel panel-default">
							<div class="panel-body dark-green text-light">
								<div class=" text-center">
                                <div class=" text-uppercase">Total Pending Users</div>
									<div class=" h1 "><?php $sql = "select access from users where access='0'";
                                    $result = mysqli_query($con,$sql) or die(mysqli_error());
                                    echo mysqli_num_rows($result); ?></div>
									<div class=" text-uppercase links"><a href="pendinguser.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
								</div>
							</div>
						</div>
                    </div>
                    <div class="col-md-3 ">
						<div class="panel panel-default">
							<div class="panel-body dark-red text-light">
								<div class=" text-center">
                                <div class=" text-uppercase">Total Deleted Users</div>
									<div class=" h1 "><?php $sql = "select id from deletedusers";
                                    $result = mysqli_query($con,$sql) or die(mysqli_error());
                                    echo mysqli_num_rows($result); ?></div>
									<div class=" text-uppercase links"><a href="deleteduser.php">More info <i class="fa fa-arrow-circle-right"></i></a></div>
								</div>
							</div>
						</div>
					</div>
                    
				</div>
			</div>
        </section>
        <footer class="panel-footer">
    <div class="pull-right hidden-xs">
        <label>Design By- </label> <a href="#" target="_blank">Summet Prajapati</a>
    </div>
    <strong>Copyright &copy; 2020 <a href="#">YOR Garage</a>.</strong> All rights reserved.
</footer>
      </section>

	  </section>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
  <script>
      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>

</html>
<?php } ?>