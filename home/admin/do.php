<?php
session_start();
include'dbconnection.php';
$label=$c=$loc="";
$nam=($_GET['name']);
switch ($nam) {
    case 'gst':
        $label="GST Certificate"; $c="GST_Certificate"; $loc="garagedocs/gst/";
        break;
    case 'sla':
        $label="Service Level Agreement"; $c="Service_level_agreement"; $loc="garagedocs/ServiceLevelAgreement/";
        break;
    case 'qaa':
        $label="Quality Assurance Agreement"; $c="Quality_Assurance_Agreement"; $loc="garagedocs/QualityAssuranceAgreement/";
        break;
    case 'pa':
        $label="Payment Agreement"; $c="Payment_Agreement"; $loc="garagedocs/PaymentAgreement/";
        break;
    case 'pan':
        $label="Owner PAN"; $c="Owner_PAN"; $loc="garagedocs/PAN/";
        break;
    case 'aa':
        $label="Owner Adhaar"; $c="Owner_Adhaar"; $loc="garagedocs/Adhaar/";
        break; 
    case 'bcc':
        $label="Bank Cancelled Cheque"; $c="Bank_Cancelled_Cheque"; $loc="garagedocs/CancelledCheque/";
        break;
    case 'nda':
        $label="Non Disclosure Agreement"; $c="Non_disclosure_agreement"; $loc="garagedocs/NDA/";
        break;                       
    default:
    $label=$c=$loc="";
        break;
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin | Update Profile</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <style>
        th,td{ 
            border-color: black;
        width: 10px; 
        text-align: center; 
        }
        </style>
  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
              <a href="#" class="logo"><b>Admin Dashboard</b></a>
            
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="logout.php">Logout</a></li>
            	</ul>
            </div>
        </header>
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="#"><img src="assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php echo $_SESSION['login'];?></h5>
              	  	
                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="manage-users.php" >
                          <i class="fa fa-users"></i>
                          <span>Manage Users</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                          <i class="fa fa-users"></i>
                          <span>bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="garages.php">
                          <i class="fa fa-users"></i>
                          <span>Garages</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="suppliers.php">
                          <i class="fa fa-users"></i>
                          <span>Suppliers</span>
                      </a>
                  </li>
              
                 
              </ul>
          </div>
      </aside>
     
      <section id="main-content">
          <section class="wrapper">
          <h3><i class="fa fa-angle-right"></i> <?php echo $_SESSION['name'];?>'s Documents</h3>
             	
				<div class="row">
	                  
                  <div class="col-md-12">
                      <div class="content-panel">
                          <div class="panel-body">
                     <form enctype='multipart/form-data' method="post" action="" class="">
                     <div class="table-responsive text-nowrap">
                       <table class="table table-bordered table-striped table-hover">
                       <thead class=" thead-light" >
                        
                           <tr>
                               
                        <th scope="col">SrNO.
                        </th>
                        <th scope="col">Documents
                        </th>
                        <th scope="col">Select Files
                        </th>
                        <th scope="col">Starting Date
                        </th>
                        <th scope="col">Ending date
                        </th>
                           </tr>
                        </thead>
                       <tbody>
                       <tr>
                       <th scope="row">1</th>
                       <td>
                        <?php echo $label?>:
                        </td>
                        <td>
                        <input type="file" name="file1" id="file" accept="image/*" required>
                        </td>
                        <td>
                        <input type="date" class="ui-datepicker" >
                       </td>
                        
                       <td>
                       <input type="date" class="ui-datepicker-group" >
                       </td>
                        </tr>
                        
                       </tbody>
                        </table>
                        <input type="submit" value="Update File" name="submit" class=" btn btn-success left-padding"/>
                        
                       
                    </div>
                     </form>
                    </div>
                      </div>
                  </div>
              </div>
		</section>
       
      </section>
    </section>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
  <script>
      $(function(){
          $('select.styled').customSelect();
      });
     
  </script>

  </body>
</html>
<?php


if (isset($_POST['submit']))
{
    $file1= $_FILES["file1"]["name"];
        $target_file = $loc . basename($file1);
         move_uploaded_file($_FILES["file1"]["tmp_name"], $target_file);
          $sql = "UPDATE mechanic SET $c ='$file1' where id='".$_SESSION['uid']."' ";
           if(mysqli_query($con, $sql))
               {
                $_SESSION['msg']="Image successufully updated!!!";
                $URL="profile.php";
                echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
               }
             else
               {
                  $_SESSION['msg']="error in storing in database!!!";
                  $URL="profile.php";
                echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
                }
    
}


?>
