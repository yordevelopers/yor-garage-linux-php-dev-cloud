<?php
session_start();
if (strlen($_SESSION['id']==0)) {
    header('location:logout.php');
    }
    if(strlen($_POST['submit']=="")){
        header('location:bookings.php');
        }
        $bid=$_POST['submit'];
$_SESSION['b']=$bid;
$con = mysqli_connect("localhost", "root", "", "demo");
$query = "SELECT * FROM `bookings` WHERE bid='".$bid."'";
$rs = mysqli_query($con,$query); $get = mysqli_fetch_assoc($rs);
$stat=$get["garage_assign"];
if(strlen($stat!="")){header('location:newbookings.php');}
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin | Dashboard</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
    <script>
 
$(document).ready(function(){
    $("form").submit(function(){
 if ($('input:checkbox').filter(':checked').length < 1){
        alert("Check at least one!");
 return false;
 }
    });
});
</script>
  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original="Toggle Navigation"></div>
              </div>
            <a href="#" class="logo"><b>Dashboard</b></a>

            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                <li class="dropdown">
                    <a class="dropdown-toggle logout" data-toggle="dropdown" role="button" aria-expanded="false" href="#"> user</a>
            <ul class="dropdown-menu" role="menu">
            <li><a class="fa fa-power-off" href="logout.php"> logout</a></li>
            </ul>
            </li>
        </ul>
            </div>
        </header>

        <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">

              	  <p class="centered"><a href="#"><img src="assets/img/yor.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php echo $_SESSION['login'];?></h5>

                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="manage-users.php" >
                          <i class="fa fa-users"></i>
                          <span>Manage Users</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                          <i class="fa fa-file-text"></i>
                          <span>Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="garages.php">
                          <i class="fa fa-gears"></i>
                          <span>Garages</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="suppliers.php">
                          <i class="fa fa-link"></i>
                          <span>Suppliers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="retailer.php">
                          <i class="fa fa-gear"></i>
                          <span>Retailers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="distributer.php">
                          <i class="fa fa-gear"></i>
                          <span>Distributers</span>
                      </a>
                  </li>


              </ul>
          </div>
      </aside>
      <section id="main-content">
          <section class="wrapper">
          	<br>
                <div class="content-panel col-xs-12   ">
                <h3 ><i class="fa fa-angle-right"></i> JOB CARD <hr></h3>
                <p style="color:#F00"> <?php
                    if(isset($_SESSION["msg"])){
                        $error = $_SESSION["msg"];
                        echo "<span>$error</span>";
                    }
                ?>        </p>
                <div>
                <form method="POST" action="job.php">
                <table class="table table-bordered ">
                    <tr>
                     <td>Car No:</td><td><?php echo $get["car_no"];?></td><td>Car :</td><td><?php echo $get["car_brand"];echo " ";echo $get['car_model'];echo " ";echo$get['car_variant'];?></td></tr>
                    <tr><td>Mobile No:</td><td><?php echo $get["b_mobile"];?></td><td>Delivery Date:</td><td><input type="date" required id="deliverydate" name="deliverydate" onblur="compare();" ></td></tr>
                    <tr><td>Service Date Alloted:</td><td><input type="date" id="servicedate"  name="servicedate" onblur="compare();" ></td><td>Service Time Alloted:</td><td><input type="text" placeholder="24:00" pattern="([0-1]{1}[0-9]{1}|20|21|22|23):[0-5]{1}[0-9]{1}" required name="servicetime" ></td></tr>
                <tr><td>Assign Garage</td>
                   <td> <select name="type" id="type" class="custom-select sources" placeholder="Source" required>
                   <option value="">Choose...</option>
                        <?php
                $re=mysqli_query($con,'SELECT g_name FROM mechanic');
                while($ro=mysqli_fetch_assoc($re)){
                    echo "<option value='$ro[g_name]'>$ro[g_name]</option>";
                }
            ?>
                    </select>  </td>
                </tr>
                </table>
                <table class="table table-bordered centered ">
                <div class="form-group options">
                    <tr><th>SELECT</th><th>SERVICE TYPE</th><th>DESCRIPTION</th><th>PROBLEM</th></tr>
                    <tr><td><input type="checkbox" name="c[0]" id="c-1" >&nbsp;&nbsp;1</td><td><label name="basic"value="basic">Basic</label></td><td><textarea name="service_1"  cols="25" rows="4"></textarea></td><td><textarea name="problem_1"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c[1]" id="c-2" >&nbsp;&nbsp;2</td><td>Brakes</td><td><textarea name="service_2"  cols="25" rows="4"></textarea></td><td><textarea name="problem_2"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c[2]" id="c-3" >&nbsp;&nbsp;3</td><td>Suspension</td><td><textarea name="service_3"  cols="25" rows="4"></textarea></td><td><textarea name="problem_3"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c[3]" id="c-4" >&nbsp;&nbsp;4</td><td>Transmission</td><td><textarea name="service_4"  cols="25" rows="4"></textarea></td><td><textarea name="problem_4"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c[4]" id="c-5" >&nbsp;&nbsp;5</td><td>Engine</td><td><textarea name="service_5"  cols="25" rows="4"></textarea></td><td><textarea name="problem_5"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c[5]" id="c-6" >&nbsp;&nbsp;6</td><td>Body Parts</td><td><textarea name="service_6"  cols="25" rows="4"></textarea></td><td><textarea name="problem_6"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c[6]" id="c-7" >&nbsp;&nbsp;7</td><td>Electrical/AC</td><td><textarea name="service_7"  cols="25" rows="4"></textarea></td><td><textarea name="problem_7"  cols="25" rows="4"></textarea></td></tr>
               
                </table>
                <input class="btn btn-success"  type="submit" name="submit" value="Submit" >
                </div>
            </form>
            </div>
			</div>
        </section>
        <footer class="panel-footer">
    <div class="pull-right hidden-xs">
        <label>Design By- </label> <a href="#" target="_blank">Summet Prajapati</a>
    </div>
    <strong>Copyright &copy; 2020 <a href="#">YOR Garage</a>.</strong> All rights reserved.
</footer>
      </section>

      </section>
      
      <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
    <script>
      
// var dd = String(today.getDate()).padStart(2, '0');
// var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
// var yyyy = today.getFullYear();

// today = mm + '/' + dd + '/' + yyyy;
function compare()
{  var today = new Date();
    var service = document.getElementById("servicedate").value;
    var delivery = document.getElementById("deliverydate").value;
    if((new Date(service).getTime() > new Date(today).getTime()) || (new Date(delivery).getTime() > new Date(today).getTime()) )
    {
       
        $(':input[type="submit"]').prop('disabled', false); 
    }
    else{
        alert("wrong Service or delivery date selected");
        $(':input[type="submit"]').prop('disabled', true); 
    }
    
}
     $(function(){
          $('select.styled').customSelect();
      });
</script>
</body>
</html>
<?php
    unset($_SESSION["msg"]);
?>