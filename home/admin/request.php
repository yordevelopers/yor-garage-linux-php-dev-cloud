<?php
session_start();
include'dbconnection.php';
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin | Dashboard</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
    <style>

    </style>

  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original="Toggle Navigation"></div>
              </div>
            <a href="#" class="logo"><b>Admin Dashboard</b></a>

            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout fa fa-power-off" href="logout.php"></a></li>
            	</ul>
            </div>
        </header>

        <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">

              	  <p class="centered"><a href="#"><img src="assets/img/yor.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php echo $_SESSION['login'];?></h5>

                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="manage-users.php" >
                          <i class="fa fa-users"></i>
                          <span>Manage Users</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                          <i class="fa fa-file-text"></i>
                          <span>Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="garages.php">
                          <i class="fa fa-gears"></i>
                          <span>Garages</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="suppliers.php">
                          <i class="fa fa-link"></i>
                          <span>Suppliers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="retailer.php">
                          <i class="fa fa-gear"></i>
                          <span>Retailers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="distributer.php">
                          <i class="fa fa-gear"></i>
                          <span>Distributers</span>
                      </a>
                  </li>


              </ul>
          </div>
      </aside>
      <section id="main-content">
          <section class="wrapper">
          	<br>
                <div class="content-panel col-xs-12   ">
                <h3 ><i class="fa fa-angle-right"></i> Dashboard <hr></h3>
                <div>
                <form action="job.php" method="POST">
                <table class="table table-bordered ">
                    <tr>
                     <td>Car No:</td><td><input type="text" required name="carno" ></td><td>Car Model:</td><td><input type="text" required name="carmodel" ></td></tr>
                    <tr><td>Mobile No:</td><td><input type="text" required name="mobileno" ></td><td>Delivery Date:</td><td><input type="date" required name="deliverydate" ></td></tr>
                    <tr><td>Service Date Alloted:</td><td><input type="date" required name="servicedate" ></td><td>Service Time Alloted:</td><td><input type="time" required name="servicetime" ></td></tr>
                </table>
                <table class="table table-bordered centered ">
                    <tr><th>SELECT</th><th>SERVICE TYPE</th><th>DESCRIPTION</th><th>PROBLEM</th></tr>
                    <tr><td><input type="checkbox" name="c1"></td><td>Basic</td><td><textarea name="service_1"  cols="25" rows="4"></textarea></td><td><textarea name="problem_1"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c2"></td><td>Brakes</td><td><textarea name="service_2"  cols="25" rows="4"></textarea></td><td><textarea name="problem_2"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c3"></td><td>Suspension</td><td><textarea name="service_3"  cols="25" rows="4"></textarea></td><td><textarea name="problem_3"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c4"></td><td>Transmission</td><td><textarea name="service_4"  cols="25" rows="4"></textarea></td><td><textarea name="problem_4"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c5"></td><td>Engine</td><td><textarea name="service_5"  cols="25" rows="4"></textarea></td><td><textarea name="problem_5"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c6"></td><td>Body Parts</td><td><textarea name="service_6"  cols="25" rows="4"></textarea></td><td><textarea name="problem_6"  cols="25" rows="4"></textarea></td></tr>
                    <tr><td><input type="checkbox" name="c7"></td><td>Electrical/AC</td><td><textarea name="service_7"  cols="25" rows="4"></textarea></td><td><textarea name="problem_7"  cols="25" rows="4"></textarea></td></tr>
                </table>
                <input class="btn btn-success" type="submit" name="submit" value="Submit">
            </form>
            </div>
			</div>
        </section>
        <footer class="panel-footer">
    <div class="pull-right hidden-xs">
        <label>Design By- </label> <a href="#" target="_blank">Summet Prajapati</a>
    </div>
    <strong>Copyright &copy; 2020 <a href="#">YOR Garage</a>.</strong> All rights reserved.
</footer>
      </section>

	  </section>
      <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
    <script>
     $(function(){
          $('select.styled').customSelect();
      });
</script>
</body>
</html>
