<?php
session_start();
$conn = mysqli_connect("localhost", "root", "", "demo");
$results = mysqli_query($conn, "SELECT * FROM mechanic where id='".$_SESSION['uid']."'");
$users = mysqli_fetch_all($results, MYSQLI_ASSOC);
foreach ($users as $row):
   $n=$row['m_name'];
   $_SESSION['name']=$n;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin | Update Profile</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <style>
        table{
        margin-left:5px;
        }
        th{ 
            
            width: 100px; 
            text-align: center; 
           
            }
        td{ 
            
        width: 200px; 
        text-align: center; 
       
        }
        tr{
           
  
        }
        </style>
  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
              <a href="#" class="logo"><b>Admin Dashboard</b></a>
            
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="logout.php">Logout</a></li>
            	</ul>
            </div>
        </header>
        <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">

              	  <p class="centered"><a href="#"><img src="assets/img/yor.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php echo $_SESSION['login'];?></h5>

                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="manage-users.php" >
                          <i class="fa fa-users"></i>
                          <span>Manage Users</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                          <i class="fa fa-file-text"></i>
                          <span>Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="garages.php">
                          <i class="fa fa-gears"></i>
                          <span>Garages</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="suppliers.php">
                          <i class="fa fa-link"></i>
                          <span>Suppliers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="retailer.php">
                          <i class="fa fa-gear"></i>
                          <span>Retailers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="distributer.php">
                          <i class="fa fa-gear"></i>
                          <span>Distributers</span>
                      </a>
                  </li>


              </ul>
          </div>
      </aside>
     
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> <?php echo $row['m_name'];?>'s Documents</h3>
             	
				<div class="row">
	                  
                  <div class="col-md-12 ">
                      <div class="content-panel">
                          <div class="panel-body">

                          <div id="success"><p style="color:#F00"> <?php
                    if(isset($_SESSION["msg"])){
                        $error = $_SESSION["msg"];
                        echo "<span>$error</span>";
                    }
                ?>        </p></div>
                          
                          <div class="table-responsive text-nowrap">
                          <table class="table table-bordered table-striped table-hover ">
                          <thead class=" thead-light" >
                        <tr>
                     <th scope="col">SrNO.
                     </th>
                     <th scope="col">Documents Names
                     </th>
                     <th scope="col">Documents Images
                     </th>
                     
                        </tr>
                     </thead>
                     <tbody>
                            <tr><th>1)</th><td>GST Certificate</td> <td><a href="<?php echo 'garagedocs/gst/'.$row['GST_Certificate']; ?>"><img src="<?php echo 'garagedocs/gst/'.$row['GST_Certificate']; ?>" width="100" height="100" alt="allll"></a> <a href="do.php?name=gst"><i class="fa fa-pencil"></i></a></td></tr>
                            <tr> <th>2)</th><td>Service Level Agreement</td><td><a href="<?php echo 'garagedocs/'.$row['Service_level_agreement']; ?>" > <img src="<?php echo 'garagedocs/'.$row['Service_level_agreement']; ?>" width="100" height="100" alt="file not found"></a><a href="do.php?name=sla"><i class="fa fa-pencil"></i></a></td></tr>
                           <tr> <th>3)</th><td>Quality Assurance Agreement</td><td><a href="<?php echo 'garagedocs/'.$row['Quality_Assurance_Agreement']; ?>"> <img src="<?php echo 'garagedocs/'.$row['Quality_Assurance_Agreement']; ?>" width="100" height="100" alt="file not found"></a><a href="do.php?name=qaa"><i class="fa fa-pencil"></i></a></td></tr>
                           <tr><th>4)</th><td>Payment Agreement</td><td> <a href="<?php echo 'garagedocs/'.$row['Payment_Agreement']; ?>"><img src="<?php echo 'garagedocs/'.$row['Payment_Agreement']; ?>" width="100" height="100" alt="file not found"></a> <a href="do.php?name=pa"><i class="fa fa-pencil"></i></a></td></tr>
                           <tr> <th>5)</th><td>Owner PAN</td><td><a href="<?php echo 'garagedocs/PAN/'.$row['Owner_PAN']; ?>"> <img src="<?php echo 'garagedocs/PAN/'.$row['Owner_PAN']; ?>" width="100" height="100" alt="file not found"></a> <a href="do.php?name=pan"><i class="fa fa-pencil"></i></a></td></tr>
                           <tr> <th>6)</th><td>Owner Adhaar</td> <td> <a href="<?php echo 'garagedocs/'.$row['Owner_Adhaar']; ?>"><img src="<?php echo 'garagedocs/'.$row['Owner_Adhaar']; ?>" width="100" height="100" alt="file not found"></a><a href="do.php?name=aa"><i class="fa fa-pencil"></i></a></td></tr>
                           <tr><th>7)</th><td>Bank Cancelled Cheque</td> <td><a href="<?php echo 'garagedocs/'.$row['Bank_Cancelled_Cheque']; ?>"> <img src="<?php echo 'garagedocs/'.$row['Bank_Cancelled_Cheque']; ?>" width="100" height="100" alt="file not found"></a> <a href="do.php?name=bcc"><i class="fa fa-pencil"></i></a></td></tr>
                           <tr><th>8)</th><td>Non Disclosure Agreement</td><td><a href="<?php echo 'garagedocs/NDA/'.$row['Non_disclosure_agreement']; ?>"> <img src="<?php echo 'garagedocs/NDA/'.$row['Non_disclosure_agreement']; ?>" width="100" height="100" alt="file not found"></a> <a href="do.php?name=nda"><i class="fa fa-pencil"></i></a></td></tr>
                        </tbody>
                        <?php endforeach;?>

                        </table>
                        </div>
                    </form>
                    </div>
                      </div>
                  </div>
              </div>
		</section>
       
      </section>
    </section>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
  <script>
      $(function(){
          $('select.styled').customSelect();
      });
      setTimeout(function() {
    $('#success').fadeOut('fast');
}, 5000); // <-- time in milliseconds
     </script>

  </body>
</html>
<?php



unset($_SESSION["msg"]);
?>