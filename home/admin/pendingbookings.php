<?php
session_start();
include'dbconnection.php';
// checking session is valid for not
if (strlen($_SESSION['id']==0)) {
  header('location:logout.php');
  } else{

// for deleting user

?><!DOCTYPE html>
<html lang="en">
  <head>
   
    <title>Admin | Manage GARAGE</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    <script type="text/javascript">
$(document).on('click', '.delete', function(){
        var id = this.id;
				$.ajax({
					url:"delete.php",
					method: "POST",
					data:{bid:id},
					success:function()
					{
                        $('tr#'+id+'').css('background-color', '#ccc');
                        $('tr#'+id+'').fadeOut('slow');
					}
				});
});
</script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />  
  </head>
  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <a href="#" class="logo"><b>Admin Dashboard</b></a>

            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="logout.php">Logout</a></li>
            	</ul>
            </div>
        </header>
        <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">

              	  <p class="centered"><a href="#"><img src="assets/img/yor.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php echo $_SESSION['login'];?></h5>

                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="manage-users.php" >
                          <i class="fa fa-users"></i>
                          <span>Manage Users</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                          <i class="fa fa-file-text"></i>
                          <span>Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="garages.php">
                          <i class="fa fa-gears"></i>
                          <span>Garages</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="suppliers.php">
                          <i class="fa fa-link"></i>
                          <span>Suppliers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="retailer.php">
                          <i class="fa fa-gear"></i>
                          <span>Retailers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="distributer.php">
                          <i class="fa fa-gear"></i>
                          <span>Distributers</span>
                      </a>
                  </li>


              </ul>
          </div>
      </aside>
      <section id="main-content">
          <section class="wrapper">
          <br>
              <div class="content-panel col-md-12 ">
          	<h3><i class="fa fa-angle-right"></i>Garage Confirmation Pending Bookings</h3>
				<div class="row">



                  <div class="col-md-12">
                      <div class="table-responsive">
                      <table id="employee_data" class="table table-striped table-bordered">

                              <thead>
                              <tr>
                              <th>Booking ID</th>
                                <th>Name</th>
                                <th>Mobile No.</th>
                                <th>Car Brand</th>
                                <th>Car Model</th>
                                <th>Car Variant</th>
                                <th>Car No.</th>
                                <th>Status</th>
                                <th>Assigned Garage</th>
                                <th>Booking Date</th>
                                <!-- <th>ADD DETAILS</th>
                                <th>DELETE</th> -->

                              </tr>
                              </thead>
                              <tbody>
                              <?php $ret=mysqli_query($con,"SELECT * FROM bookings where garage_assign='assigned' and status='pending'");
							  
							  while($row=mysqli_fetch_array($ret))
							  {?>
                              <tr id="<?php echo $row["bid"]; ?>">
                                  <td><?php echo $row['bid'];?></td>
                                  <td><?php echo $row['b_name'];?></td>
                                  <td><?php echo $row['b_mobile'];?></td>
                                  <td><?php echo $row['car_brand'];?></td>
                                  <td><?php echo $row['car_model'];?></td>
                                  <td><?php echo $row['car_variant'];?></td>
                                  <td><?php echo $row['car_no'];?></td>
                                  <td><?php echo $row['status'];?></td>
                                  <td><?php echo $row['garage_assign'];?></td>
                                  <td><?php echo $row['b_date'];?></td>
                                  <!-- <td>
                                   <center><a href="serviceform.php?bid=<?php// echo $row['bid'];?>"><button class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></button></a></center>
                                  </td>
                                  <td>  <center> <button type="button" class="delete btn-danger" id="<?php //echo $row["bid"]; ?>"><i class="fa fa-trash-o "></i></button></center></td> -->
                              </tr>
                              <?php }?>
                              </tbody>
                          </table>
                          <div style="margin-left:100px;">
                          <a class="btn btn-theme" href = "javascript:history.back()" ><i class="fa fa-backward" ></i> Back</a>
                        </div>
                      </div>
                  </div>
              </div>
                              </div>
		</section>
      </section>
	  </section>
    
</body>
  </html>
  <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
    
<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 $(function(){
          $('select.styled').customSelect();
      });
 </script>
<?php } ?>
