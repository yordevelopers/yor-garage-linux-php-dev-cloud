<?php
session_start();
include'dbconnection.php';
$uid=intval($_GET['uid']);
$_SESSION['uid']=$uid;
$conn = mysqli_connect("localhost", "root", "", "demo");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin | Update Profile</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <style>
        th,td{ 
            border-color: black;
        width: 10px; 
        text-align: center; 
        }
        </style>
  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
              <a href="#" class="logo"><b>Admin Dashboard</b></a>
            
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="logout.php">Logout</a></li>
            	</ul>
            </div>
        </header>
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="#"><img src="assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php echo $_SESSION['login'];?></h5>
              	  	
                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="manage-users.php" >
                          <i class="fa fa-users"></i>
                          <span>Manage Users</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                          <i class="fa fa-users"></i>
                          <span>bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="garages.php">
                          <i class="fa fa-users"></i>
                          <span>Garages</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="suppliers.php">
                          <i class="fa fa-users"></i>
                          <span>Suppliers</span>
                      </a>
                  </li>
              
                 
              </ul>
          </div>
      </aside>
     
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Upload Documents</h3>
             	
				<div class="row">
	                  
                  <div class="col-md-12">
                      <div class="content-panel">
                          <div class="panel-body">
                     <form enctype='multipart/form-data' method="post" action="" class="">
                     <div class="table-responsive text-nowrap">
                       <table class="table table-bordered table-striped">
                       <thead class=" thead-light" >
                        
                           <tr>
                               
                        <th scope="col">SrNO.
                        </th>
                        <th scope="col">Documents
                        </th>
                        <th scope="col">Select Files
                        </th>
                        <th scope="col">Uploaded Documents
                        </th>
                        <th scope="col">Starting Date
                        </th>
                        <th scope="col">Ending date
                        </th>
                           </tr>
                        </thead>
                       <tbody>
                       <tr>
                       <th scope="row">1</th>
                       <td>
                        GST-Certificate:
                        </td>
                        <td>
                        <input type="file" name="file1" id="file">
                        </td>
                        <td>
                        <?php
                        $q = "SELECT `GST_Certificate` FROM mechanic WHERE id='".$uid."'";
                        $result = $conn->query($q);
                        $row = $result->fetch_assoc();
                        $temp1 = $row["GST_Certificate"];
                        if($temp1 == "")
                        {
                            echo " &nbsp;No files Present";
                        }
                        else {
                            echo $temp1;
                        }
                        ?>
                        </td>
                        <td>
                        <input type="date" class="ui-datepicker" >
                       </td>
                        
                       <td>
                       <input type="date" class="ui-datepicker-group" >
                       </td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                       <td>
                        GST-Certificate:
                        </td>
                        <td>
                        <input type="file" name="file2" id="file">
                        </td>
                        <td>
                        <?php
                        $q = "SELECT `GST_Certificate` FROM mechanic WHERE id='".$uid."'";
                        $result = $conn->query($q);
                        $row = $result->fetch_assoc();
                        $temp2 = $row["GST_Certificate"];
                        if($temp2 == "")
                        {
                            echo " &nbsp;No files Present";
                        }
                        else {
                            echo $temp2;
                        }
                        ?>
                        </td>
                        <td>
                        <input type="date" class="ui-datepicker" >
                       </td>
                        
                       <td>
                       <input type="date" class="ui-datepicker-group" >
                       </td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                       <td>
                        GST-Certificate3:
                        </td>
                        <td>
                        <input type="file" name="file3" id="file" class="form-control-file">
                        </td>
                        <td>
                        <?php
                        
                        $q = "SELECT `GST_Certificate` FROM mechanic WHERE id='".$uid."'";
                        $result = $conn->query($q);
                        $row = $result->fetch_assoc();
                        $temp3 = $row["GST_Certificate"];
                        if($temp3 == "")
                        {
                            echo "&nbsp;No files Present";
                        }
                        else {
                            echo $temp3;
                        }
                        ?>
                        </td>                       
                        <td>
                        <input type="date" class="ui-datepicker" >
                       </td>
                        
                       <td>
                       <input type="date" class="ui-datepicker-group" >
                       </td>
                       </tr>
                       </tbody>
                        </table>
                        <input type="submit" value="Upload Files" name="submit" class=" btn btn-success left-padding"/>
                        
                       
                    </div>
                     </form>
                    </div>
                      </div>
                  </div>
              </div>
		</section>
       
      </section>
    </section>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
  <script>
      $(function(){
          $('select.styled').customSelect();
      });
      
  </script>

  </body>
</html>
<?php

$conn = mysqli_connect("localhost", "root", "", "demo");
if (isset($_POST['submit']))
{
    $countfiles = count($_FILES['arr']['name']);
 
 // Looping all files
 for($i=0;$i<$countfiles;$i++){
   $filename = $_FILES['arr']['name'][$i];
//     # Location
//   $location = "garagedocs/".$filename;

  # create directory if not exists in upload/ directory
//   if(!is_dir($location)){
//     mkdir($location, 0755);
//   }

//   $location .= "/".$filename;

   // Upload file
   move_uploaded_file($_FILES['arr']['tmp_name'][$i],'garagedocs/'.$filename);
    
 }
}
?>