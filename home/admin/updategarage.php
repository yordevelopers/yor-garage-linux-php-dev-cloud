<?php
session_start();
include'dbconnection.php';
$uid=intval($_GET['uid']);
$_SESSION['uid']=$uid;
$conn = mysqli_connect("localhost", "root", "", "demo");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin | Update Profile</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <style>
        th,td{ 
            border-color: black;
        width: 10px; 
        text-align: center; 
        }
        </style>
  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
              <a href="#" class="logo"><b>Admin Dashboard</b></a>
            
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="logout.php">Logout</a></li>
            	</ul>
            </div>
        </header>
        <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">

              	  <p class="centered"><a href="#"><img src="assets/img/yor.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php echo $_SESSION['login'];?></h5>

                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="manage-users.php" >
                          <i class="fa fa-users"></i>
                          <span>Manage Users</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                          <i class="fa fa-file-text"></i>
                          <span>Bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="garages.php">
                          <i class="fa fa-gears"></i>
                          <span>Garages</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="suppliers.php">
                          <i class="fa fa-link"></i>
                          <span>Suppliers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="retailer.php">
                          <i class="fa fa-gear"></i>
                          <span>Retailers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="distributer.php">
                          <i class="fa fa-gear"></i>
                          <span>Distributers</span>
                      </a>
                  </li>


              </ul>
          </div>
      </aside>
     
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Upload Documents</h3>
             	
				<div class="row">
	                  
                  <div class="col-md-12">
                      <div class="content-panel">
                          <div class="panel-body">
                     <form enctype='multipart/form-data' method="post" action="" class="">
                     <div class="table-responsive text-nowrap">
                       <table class="table table-bordered table-striped table-hover">
                       <thead class=" thead-light" >
                        
                           <tr>
                               
                        <th scope="col">SrNO.
                        </th>
                        <th scope="col">Documents
                        </th>
                        <th scope="col">Select Files
                        </th>
                        <th scope="col">Starting Date
                        </th>
                        <th scope="col">Ending date
                        </th>
                           </tr>
                        </thead>
                       <tbody>
                       <tr>
                       <th scope="row">1</th>
                       <td>
                        GST-Certificate:
                        </td>
                        <td>
                        <input type="file" name="file1" id="file" required>
                        </td>
                       
                        </td>
                        <td>
                        <input type="date" class="ui-datepicker" >
                       </td>
                        
                       <td>
                       <input type="date" class="ui-datepicker-group" >
                       </td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                       <td>
                        GST-Certificate:
                        </td>
                        <td>
                        <input type="file" name="file2" id="file" required>
                        </td>
                        
                        <td>
                        <input type="date" class="ui-datepicker" >
                       </td>
                        
                       <td>
                       <input type="date" class="ui-datepicker-group" >
                       </td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                       <td>
                        GST-Certificate3:
                        </td>
                        <td>
                        <input type="file" name="file3" id="file" required>
                        </td>
                        
                        <td>
                        <input type="date" class="ui-datepicker" >
                       </td>
                        
                       <td>
                       <input type="date" class="ui-datepicker-group" >
                       </td>
                       </tr>
                       </tbody>
                        </table>
                        <input type="submit" value="Upload Files" name="submit" class=" btn btn-success left-padding"/>
                        
                       
                    </div>
                     </form>
                    </div>
                      </div>
                  </div>
              </div>
		</section>
       
      </section>
    </section>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
  <script>
      $(function(){
          $('select.styled').customSelect();
      });
      function compare()
{
    var startDt = document.getElementById("startDate").value;
    var endDt = document.getElementById("endDate").value;

    
}
  </script>

  </body>
</html>
<?php

$conn = mysqli_connect("localhost", "root", "", "demo");
if (isset($_POST['submit']))
{
    $file1= $_FILES["file1"]["name"];
    $file2= $_FILES["file2"]["name"];
    $file3= $_FILES["file3"]["name"];
    if(!empty($file1)){
        $target_dir = "garagedocs/gst/";
        $target_file = $target_dir . basename($file1);
         move_uploaded_file($_FILES["file1"]["tmp_name"], $target_file);
          $sql = "UPDATE mechanic SET GST_Certificate ='$file1' where id='".$uid."' ";
           if(mysqli_query($conn, $sql))
               {
                 
               }
             else
               {
                   $message = "There was an error in the database";
                   echo "<script type='text/javascript'>alert('$message');</script>";
                }
    }
    else {
        }  
        if(!empty($file2)){
            $target_dir = "garagedocs/NDA/";
            $target_file = $target_dir . basename($file2);
             move_uploaded_file($_FILES["file2"]["tmp_name"], $target_file);
              $sql = "UPDATE mechanic SET Non_disclosure_agreement ='$file2' where id='".$uid."' ";
               if(mysqli_query($conn, $sql))
                   {
                     
                   }
                 else
                   {
                       $message = "There was an error in the database";
                       echo "<script type='text/javascript'>alert('$message');</script>";
                    }
        }
        else {
            }        
            if(!empty($file3)){
                $target_dir = "garagedocs/PAN/";
                $target_file = $target_dir . basename($file3);
                 move_uploaded_file($_FILES["file3"]["tmp_name"], $target_file);
                  $sql = "UPDATE mechanic SET Owner_PAN ='$file3' where id='".$uid."' ";
                   if(mysqli_query($conn, $sql))
                       {
                         
                       }
                     else
                       {
                           $message = "There was an error in the database";
                           echo "<script type='text/javascript'>alert('$message');</script>";
                        }
            }
            else {
                } 
                $message = "files Uploaded";
               echo "<script type='text/javascript'>alert('$message');</script>";             
}
    
         

?>