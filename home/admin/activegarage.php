<?php
session_start();
include'dbconnection.php';
// checking session is valid for not
if (strlen($_SESSION['id']==0)) {
  header('location:logout.php');
  } else{

// for deleting user
if(isset($_GET['id']))
{
$adminid=$_GET['id'];
$msg=mysqli_query($con,"delete from mechanic where id='$adminid'");
if($msg)
{
    header("location:activegarage.php");
}
}
?><!DOCTYPE html>
<html lang="en">
  <head>
   
    <title>Admin | Active Garages</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />  

    
  </head>

  <body>

  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <a href="#" class="logo"><b>Admin Dashboard</b></a>

            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="logout.php">Logout</a></li>
            	</ul>
            </div>
        </header>
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">

              	  <p class="centered"><a href="#"><img src="assets/img/yor.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php echo $_SESSION['login'];?></h5>

                    <li class="mt">
                      <a href="dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="change-password.php">
                          <i class="fa fa-lock"></i>
                          <span>Change Password</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="manage-users.php" >
                          <i class="fa fa-users"></i>
                          <span>Manage Users</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="bookings.php">
                          <i class="fa fa-users"></i>
                          <span>bookings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="garages.php">
                          <i class="fa fa-users"></i>
                          <span>Garages</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="suppliers.php">
                          <i class="fa fa-users"></i>
                          <span>Suppliers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="retailer.php">
                          <i class="fa fa-users"></i>
                          <span>retailers</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="distributer.php">
                          <i class="fa fa-users"></i>
                          <span>Distributers</span>
                      </a>
                  </li>
              </ul>
          </div>
      </aside>
      <section id="main-content">
          <section class="wrapper">
          <br>
              <div class="content-panel col-md-12 ">
          	<h3><i class="fa fa-angle-right"></i> Total Active Garages</h3>
				<div class="row">
                  <div class="col-md-12">
                  <div class=" table-responsive">
                  <table id="employee_data" class="table table-striped table-bordered">
                              <thead>
                              <tr>
                                  <th>Sno.</th>
                                  <th>Garage Name</th>
                                  <th>Password</th>
                                  <th>Email</th>
                                  <th>Contact no.</th>
                                  <th>Garage Verified</th>
                                  <!-- <th>Document Verified</th> -->
                                  <th>Delete/Edit</th>
                                  


                              </tr>
                              </thead>
                              <tbody>
                              <?php $ret=mysqli_query($con,"select * from mechanic where verified='YES' ");
							  $cnt=1;
							  while($row=mysqli_fetch_array($ret))
							  {?>
                              <tr>
                                  <td><?php echo $cnt;?></td>
                                  <td><?php echo $row['m_name'];?></td>
                                  <td><?php echo $row['m_password'];?></td>
                                  <td><?php echo $row['m_email'];?></td>
                                  <td><?php echo $row['m_contact'];?></td>
                                  <td><?php echo $row['verified'];?></td>
                                  <!-- <td><?php// echo $row['docs'];?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="update-profile.php?uid=<?php// echo $row['id'];?>">
                                     <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td> -->


                                  <td>
                                        <a href="update-profile.php?uid=<?php echo $row['id'];?>"> 
                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                        <!-- <a href="activegarage.php?id=<?php //echo $row['id'];?>"> 
                                        <button class="btn btn-danger btn-xs" onClick="return confirm('Do you really want to delete');"><i class="fa fa-trash-o "></i></button></a> -->
                                   </td>
                                </tr>
                                 <?php $cnt=$cnt+1; }?>
                                
                                 </tbody>
                             </table>
                      </div>
                  </div>
              </div>
        </div>
		</section>
      </section>
	  </section>
    

</body>
  </html>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/common-scripts.js"></script>
<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 $(function(){
          $('select.styled').customSelect();
      });

 </script>
<?php } ?>
