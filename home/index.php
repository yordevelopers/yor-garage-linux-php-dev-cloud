<?php
session_start();

if($_SESSION['email'])
{
if((time() - $_SESSION['last']) > 60) // Time in Seconds
 {
 header("location:logout.php");
 }
 else
 {
 $_SESSION['last'] = time();
 }   
}
else {
	header("Location: index.html");//redirect to login page to secure the welcome page without login access.
	
}
?>


<!DOCTYPE html>
<html>

<!--  46:37  -->
<head>
<meta charset="utf-8">
<title> Homepage</title>
<!-- Stylesheets -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 <script>
 $(function() {
    $( "#search" ).autocomplete({
        source: 'search.php'
    });
 });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
   <div id="reloader">
  <div id="status">&nbsp;</div>
</div>
 	
    <!-- Main Header-->
    <header class="main-header" >
    	
		<!-- Header Top -->
    	<div class="header-top">
        	<div class="auto-container">
            	<div class="top-outer clearfix">
                    
                    <!--Top Left-->
                    <div class="top-left">
                    	<ul class="links clearfix">
                        	<li><a href="tel:09033508015"><span class="icon fa fa-phone"></span>  Call us 9033508015</a></li>
                            <li><a href="mailto:summet.prajapati@yorcabs.co.in"><span class="icon fa fa-envelope-o"></span>summet.prajapati@yorcabs.co.in</a></li>
                        </ul>
                    </div>
                    
                   <!--Top Right-->
						  <div class="top-right clearfix">
                          <ul class="links">
							  <li> <a href="#"><?php
							  $d= " Welcome! ";
											echo $d." ".$_SESSION['email'];
											?></a></li>

							  <?php if( isset($_SESSION['email']) && !empty($_SESSION['email']) )
                                       {
                                       ?>
                                    <li>  <a href="logout.php">Logout</a></li>
                                             <?php }else{ ?>
                                         <li> <a href="login.php">Login</a></li>
                                            <li> <a href="registration.php">Register</a></li>
                                                <?php } ?>
							  </ul> 
					</div>
                    
                </div>
                
            </div>
        </div>
        <!-- Header Top End -->
		
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="header-upper-inner clearfix">
                	
                	<div class="pull-left logo-box">
                    	<div class="logo"><a href="#"><img src="images/logo.png" alt="" title=""></a></div>
                    </div>
                   	
                   	<div class="nav-outer clearfix">
                    
						<!-- Main Menu -->
						<nav class="main-menu navbar-expand-md">
							<div class="navbar-header">
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>

							<div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
								<ul class="navigation clearfix">
									<li><a href="#">Home</a>
										
									</li>
									<li class="dropdown"><a href="about.php">About</a>
										<ul>
											<li><a href="about.php">About Us</a></li>
											<!-- <li><a href="index.php">Team</a></li> -->
											<!-- <li><a href="index.php">Clients</a></li> -->
											<li><a href="faq.php">Faq</a></li>
											<!-- <li><a href="index.php">Price</a></li> -->
											<!-- <li><a href="index.php">What We Do</a></li> -->
										</ul>
									</li>
									<li><a href="services.php">Services</a></li>
									
									
									<li><a href="contactus.php">Contact us</a></li>
									
								</ul>
							</div>
							
						</nav>
						
					</div>
					
                   <!--Option Box-->
					<div class="btn-box">
						<a href="contactus.php" class="theme-btn btn-style-one">Make a Appoinment</a>
					</div>
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<a href="#" class="img-responsive"><img src="images/logo.png" alt="" title=""></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                        <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent1">
                            <ul class="navigation clearfix">
							<li><a href="#">Home</a>
										
										</li>
										<li class="dropdown"><a href="about.php">About</a>
											<ul>
												<li><a href="about.php">About Us</a></li>
												<!-- <li><a href="index.php">Team</a></li> -->
												<!-- <li><a href="index.php">Clients</a></li> -->
												<li><a href="faq.php">Faq</a></li>
												<!-- <li><a href="index.php">Price</a></li> -->
												<!-- <li><a href="index.php">What We Do</a></li> -->
											</ul>
										</li>
										<li><a href="services.php">Services</a></li>
										
										
                                        <li><a href="contactus.php">Contact us</a></li>
                                        <li> <a href="#"><?php							  
											echo $_SESSION['email'];
											?></a></li>
										
							  <?php if( isset($_SESSION['email']) && !empty($_SESSION['email']) )
                                       {
                                       ?>
                                    <li>  <a href="logout.php">Logout</a></li>
                                             <?php }else{ ?>
                                         <li> <a href="login.php">Login</a></li>
                                            <li> <a href="registration.php">Register</a></li>
                                                <?php } ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
			</div>
			<div class="progresss-container">
				<div class="progresss-bar" id="myBar"></div>
			  </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->


 <!-- 
	<header id="gtco-header" class="gtco-cover gtco-cover-md" role="banner" style="background-image: url(images/main-slider/image-1.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<h1 style="color: white;">Planing Trip To Anywhere in The World?</h1>	
						</div> -->
						<!-- <div class="col-md-4 col-md-push-1 animate-box" data-animate-effect="fadeInRight">
							<div class="form-wrap">
								<div class="tab">
									
									<div class="tab-content">
										<div class="tab-content-inner active" data-content="signup">
											<h3>Book Your Trip</h3>
											<form action="#">
												<div class="row form-group">
													<div class="col-md-12">
														<label for="fullname">Your Name</label>
														<input type="text" id="fullname" class="form-control">
													</div>
												</div>
												<div class="row form-group">
													<div class="col-md-12">
														<label for="activities">Activities</label>
														<select name="#" id="activities" class="form-control">
															<option value="">Activities</option>
															<option value="">Hiking</option>
															<option value="">Caving</option>
															<option value="">Swimming</option>
														</select>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-md-12">
														<label for="destination">Destination</label>
														<select name="#" id="destination" class="form-control">
															<option value="">Philippines</option>
															<option value="">USA</option>
															<option value="">Australia</option>
															<option value="">Singapore</option>
														</select>
													</div>
												</div>
												
												<div class="row form-group">
													<div class="col-md-12">
														<label for="date-start">Date Travel</label>
														<input type="text" id="date-start" class="form-control">
													</div>
												</div>

												<div class="row form-group">
													<div class="col-md-12">
														<input type="submit" class="btn btn-primary btn-block" value="Submit">
													</div>
												</div>
											</form>	
										</div>

										
									</div>
								</div>
							</div>
						</div>
					</div>
							
					
				</div>
			</div>
		</div>
	</header> 
			 -->			 

	<section id="cta" class="section overlay" style="background-image: url(images/main-slider/image-1.jpg);">
	
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	
		<div class="container gtco-cover">
			<div class="row">
				<div class="col-lg-4 col-md-12 ">
					<div class="newsletter-widget text-center align-self-center inner-column">
						<h2>Book Service Today!</h2>
						
						
							<form class="form-inline" method="post" action="booking.php">
								<input  id="search" pattern="[A-Za-z\s]+" title=" enter valid Car name" type="text" name="term" 
								placeholder="Enter Car Name" required class="form-control"><br/><br/>
							    <input  type="text" pattern="[0-9]{10}" title=" enter 10 digit no. only" name="mobi" placeholder="Enter mobile no." required class="form-control"><br/><br/>
								<input  type="submit" value="Book Now" class="theme-btn btn-style-one button"><br/>
								
							</form>
							<br/>
							<br/>
					</div>
				</div>
			</div>
		</div>
	</section>
	<br/>
							<br/>



	
	<!--Main Slider-->
    <!-- <section class="main-slider">
    	
        <div class="main-slider-carousel owl-carousel owl-theme">
            
            <div class="slide" style="background-image:url(images/main-slider/image-1.jpg)">
                <div class="auto-container">
					<div class="content"> -->
						<!-- <div class="rating">
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div> -->
						<!-- <h2>Welcome to YOR<span>Garage</span></h2>
					</div>
                </div>
            </div>
            
            <div class="slide" style="background-image:url(images/main-slider/image-2.jpg)">
                <div class="auto-container">
					<div class="content"> -->
						<!-- <div class="rating">
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div> -->
						<!-- <h2>Welcome to YOR<span>Garage</span></h2>
					</div>
                </div>
            </div>
			
        </div>
    </section> -->
	<!--End Main Slider-->

	
	
	<!--Services Section-->
	<section class="services-section" style="background-image:url(images/resource/image-1.png)">
		<br/>
							<br/>
							<br/>
							<br/>
		<div class="auto-container">
			
			<!--Upper Section-->
			<div class="upper-section">
				<div class="row clearfix">
					
					<!--Services Block-->
					<div class="services-block col-lg-4 col-md-6 col-sm-12">
						<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="content">
								<a href="registration.html">
								<div class="image">
									<img src="images/resource/service-1.jpg" alt="" />
									<div class="overlay-box">
										<div class="overlay-inner">
											<div class="icon-box">
												<div class="icon flaticon-funnel"></div>
											</div>
										</div>
									</div>
								</div>
							</a>
								<h3><a href="registration.html">Full Service</a></h3>
								<div class="text">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor </div>
							</div>
						</div>
					</div>
					
					<!--Services Block-->
					<div class="services-block col-lg-4 col-md-6 col-sm-12">
						<div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
							<div class="content">
								<a href="registration.html">
								<div class="image">
									<img src="images/resource/service12.jpg" alt="" />
									<div class="overlay-box">
										<div class="overlay-inner">
											<div class="icon-box">
												<div class="icon flaticon-funnel"></div>
											</div>
										</div>
									</div>
								</div>
								</a>
								<h3><a href="registration.html">AC Repair</a></h3>
								<div class="text">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor </div>
							</div>
						</div>
					</div>
										

					<!--Services Block-->
					<div class="services-block col-lg-4 col-md-6 col-sm-12">
						<div class="inner-box wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
							<div class="content">
								<a href="registration.html">
								<div class="image">
									<img  src="images/resource/service-3.jpg" alt="" />									
									  <div class="overlay-box">
										<div class="overlay-inner">
											<div class="icon-box">
												<div class="icon flaticon-funnel"></div>
											</div>
										</div>
									</div>
								</div>
								</a>
								<h3><a href="registration.html">Oil Change</a></h3>
								<div class="text">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor </div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
			<!--Lower Section-->
			<div class="lower-section">
				<div class="row clearfix">
					
					<!--Image Column-->
					<div class="image-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column">
							<div class="image">
								<img src="images/resource/orangecar.jpg" alt="" />
								<span class="icon"><img src="images/resource/duty.png" alt="" /></span>
							</div>
						</div>
					</div>
					
					<!--Content Column-->
					<div class="content-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column">
							<h2>Don’t Let Your Car Leak Oil, Make It Sweat Horse Power!</h2>
							<h3>We’ll Fix It!</h3>
							<div class="text">We are committed to quality and take car care seriously. Top-notch service is our main auto motive. </div>
							<a href="registration.html" class="theme-btn btn-style-two">read more</a>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
	</section>
	<!--End Services Section-->

	
	<!--Services Section Two-->
	<section class="services-section-two">
		<h2>Services :</h2>
		<br/>
		<br/>

		<div class="auto-container">
			<div class="row clearfix">
			
				<!-- Services Block Two -->
				<div class="services-block-two col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="icon-box">
							<span class="icon flaticon-car"></span>
						</div>
						<h3><a href="registration.html">Engine Service</a></h3>
						<div class="text"></div>
					</div>
				</div>
				
				<!-- Services Block Two -->
				<div class="services-block-two col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
						<div class="icon-box">
							<span class="icon flaticon-pressure"></span>
						</div>
						<h3><a href="registration.html">Brake Service</a></h3>
						<div class="text"></div>
					</div>
				</div>
				
				<!-- Services Block Two -->
				<div class="services-block-two col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
						<div class="icon-box">
							<span class="icon flaticon-diesel"></span>
						</div>
						<h3><a href="registration.html">Oil Change</a></h3>
						<div class="text"></div>
					</div>
				</div>
				
				<!-- Services Block Two -->
				<div class="services-block-two col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
						<div class="icon-box">
							<span class="icon flaticon-tools-2"></span>
						</div>
						<h3><a href="registration.html">AC Repair</a></h3>
						<div class="text"></div>
					</div>
				</div>
				
				<!-- Services Block Two -->
				<div class="services-block-two col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="icon-box">
							<span class="icon flaticon-car-repair"></span>
						</div>
						<h3><a href="registration.html">Full Service</a></h3>
						<div class="text"></div>
					</div>
				</div>
				
				<!-- Services Block Two -->
				<div class="services-block-two col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
						<div class="icon-box">
							<span class="icon flaticon-tyre"></span>
						</div>
						<h3><a href="registration.html">Wheel Balancing</a></h3>
						<div class="text"></div>
					</div>
				</div>
				
				<!-- Services Block Two -->
				<div class="services-block-two col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
						<div class="icon-box">
							<span class="icon flaticon-plumber"></span>
						</div>
						<h3><a href="registration.html">Basic Checkup</a></h3>
						<div class="text"></div>
					</div>
				</div>
				
				<!-- Services Block Two -->
				<div class="services-block-two col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
						<div class="icon-box">
							<span class="icon flaticon-battery"></span>
						</div>
						<h3><a href="registration.html">Battery Repair</a></h3>
						<div class="text"></div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!--End Services Section Two-->
		
	<!-- Map Section -->
	<section class="map-section">
		<div class="image-layer" style="background-image:url(images/background/1.jpg)"></div>
		<div class="auto-container">
			<h2>Your Automotive Repair Experts.<span> Where to find us?</span></h2>
			<div class="map-box" style="background-color: white;">
				<!--Map Canvas-->
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3780.4641918285693!2d73.79834531430905!3d18.6431545873377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2b932f34eac1d%3A0xaa2c39f80af5631e!2sYOR%20CABS%20-%20YRA%20TECHNOLOGIES%20PVT.%20LTD.!5e0!3m2!1sen!2sin!4v1578306002605!5m2!1sen!2sin" 
    width="1200" height="400" frameborder="3" 
    style="border:3px solid #ffffff" allowfullscreen=""></iframe>
			</div>
		</div>
	</section>
	<!-- End Map Section -->
	
	
	
	<!--Accordian Section-->
	<section class="accordian-section" style="background-image:url(images/resource/image-2.png)">
		<div class="auto-container">
			<div class="row clearfix">
				
				<!--Accordian Column-->
				<div class="accordian-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">
						
						<ul class="accordion-box accordion-style-one">
                                    
							<!--Block-->
							<li class="accordion block active-block">
								<div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-substract"></span></div>Do you provide pickup and drop facility?</div>
								<div class="acc-content current">
									<div class="content">
										<div class="text">
											<p>Yes we do provide pickup and drop facility and it is absolutely free.  </p>
										</div>
									</div>
								</div>
							</li>

							<!--Block-->
							<li class="accordion block">
								<div class="acc-btn "><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-substract"></span></div>Working Hard For The Hard Working.</div>
								<div class="acc-content">
									<div class="content">
										<div class="text">
											<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat </p>
										</div>
									</div>
								</div>
							</li>
							
							<!--Block-->
							<li class="accordion block">
								<div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-substract"></span></div> Prices Never Lower, Customer Care Never Higher.</div>
								<div class="acc-content">
									<div class="content">
										<div class="text">
											<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat </p>
										</div>
									</div>
								</div>
							</li>
			
						</ul>
						
					</div>
				</div>
				
				<!--Content Column-->
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">
						<h2>Don’t Let Your Car Leak Oil, Make It Sweat Horse Power!</h2>
						<h3>We’ll Fix It!</h3>
						<div class="text">This is Description Photoshop's version velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </div>
						<a href="about.php" class="theme-btn btn-style-three">know more</a>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!--End Accordian Section-->
	
	<!--Expert Section-->
	<!-- <section class="expert-section" style="background-image:url(images/background/2.png)">
		<div class="auto-container"> -->
			<!--Sec Title-->
			<!-- <div class="sec-title centered">
				<h2>Our Team of Experts</h2>
				<div class="text">If It Runs On Diesel…We’ll Fix It!</div>
			</div>
			
			<div class="row clearfix">
				 -->
				<!--Team Block-->
				<!-- <div class="team-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<a href="team.html"><img src="images/resource/team-1.jpg" alt="" /></a>
						</div>
						<div class="lower-content">
							<h3><a href="team.html">Jacob Reyes</a></h3>
							<div class="designation">Master Technician</div>
							<div class="text">This is Photoshop's version  of Lorem]psukroin nibh vel velit auctor aliquet.</div>
							<a href="team.html" class="theme-btn btn-style-two">read more</a>
						</div>
					</div>
				</div>
				 -->
				<!--Team Block-->
				<!-- <div class="team-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<a href="#"><img src="images/resource/team-2.jpg" alt="" /></a>
						</div>
						<div class="lower-content">
							<h3><a href="team.html">Jacob Reyes</a></h3>
							<div class="designation">Master Technician</div>
							<div class="text">This is Photoshop's version  of Lorem]psukroin nibh vel velit auctor aliquet.</div>
							<a href="team.html" class="theme-btn btn-style-two">read more</a>
						</div>
					</div>
				</div> -->
				
				<!--Team Block-->
				<!-- <div class="team-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<a href="#"><img src="images/resource/team-3.jpg" alt="" /></a>
						</div>
						<div class="lower-content">
							<h3><a href="team.html">Jacob Reyes</a></h3>
							<div class="designation">Master Technician</div>
							<div class="text">This is Photoshop's version  of Lorem]psukroin nibh vel velit auctor aliquet.</div>
							<a href="team.html" class="theme-btn btn-style-two">read more</a>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
	</section> -->
	<!--End Expert Section-->
	
	<!--Clients Section-->
    <section class="clients-section" style="background-image:url(images/background/1.jpg)">
        <div class="auto-container">
            
            <div class="sponsors-outer">
            
                 <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/2.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/3.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/4.png" alt=""></a></figure></li>
					<li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/5.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/2.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/3.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/clients/4.png" alt=""></a></figure></li>
                </ul>
            </div>
            
        </div>
    </section>  
    <!--End Clients Section-->
	
	<!--Main Footer-->
    <footer class="main-footer">
		<!--Widgets Section-->
        <div class="widgets-section">
        	<div class="auto-container">
            	<div class="row clearfix">
                
                    <!--Big Column-->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
                        <div class="row clearfix">
							
							<!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget logo-widget">
                                    <div class="logo">
                                        <a href="index.php"><img src="images/logo.png" alt="" /></a>
                                    </div>
									<div class="text">This is Photoshop's version  of Lorem]psukroin gravida nibh vel velit auctor aliquet.Aenean sollicitudin, lorem quis bibendum auctor</div>
									<ul class="social-icon-one">
										<li><a href="#" class="fa fa-twitter"></a></li>
										<li><a href="#" class="fa fa-facebook"></a></li>
										<li><a href="#" class="fa fa-google-plus"></a></li>
									</ul>
								</div>
							</div>
							
							<!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget recent-post">
									<h2>Recent Posts</h2>
									<div class="post-block">
										<div class="text"><a href="#">We’re Superior In Repair.</a></div>
										<div class="post-date">12.10.2020</div>
									</div>
									<div class="post-block">
										<div class="text"><a href="#">Experts In Diesel Motors.</a></div>
										<div class="post-date">12.10.2020</div>
									</div>
									<div class="post-block">
										<div class="text"><a href="#">We Do It Right, The First Time.</a></div>
										<div class="post-date">12.10.2020</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					
					<!--Big Column-->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
                        <div class="row clearfix">
							
							<!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget footer-links">
									<h2>Useful Links</h2>
									<ul class="links">
										<li><a href="about.php">About Us</a></li>
										<li><a href="faq.php">FAQ</a></li>
										<!-- <li><a href="#">Our Team</a></li>
										<li><a href="#">Brand</a></li>
										<li><a href="#">Ecosystem</a></li>
										<li><a href="#">Sitemap</a></li> -->
									</ul>
								</div>
							</div>
							
							<!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget contact-widget">
									<h2>Get In Contact</h2>
									<ul class="opening-time">
										<li><span>Monday:</span> 9:30 am - 6.00 pm</li>
										<li><span>Tuesday:</span> 9:30 am - 6.00 pm</li>
										<li><span>Wednesday:</span> 9:30 am - 6.00 pm</li>
										<li><span>Thursday:</span> 9:30 am - 6.00 pm</li>
										<li><span>Friday:</span> 9:30 am - 6.00 pm</li>
										<li><span>Saturday:</span> 9:30 am - 6.00 pm</li>
										<li><span>Sunday:</span>Closed</li>
									</ul>
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<!--Footer Bottom-->
        <!-- <div class="footer-bottom">
        	<div class="auto-container">
            	<div class="bottom-inner">
                    <div class="row clearfix">
                         -->
						<!--Nav Column-->
                        <!-- <div class="nav-column col-lg-6 col-md-12 col-sm-12">
							<ul class="footer-nav">
								<li><a href="#">Home</a></li>
								<li><a href="#">services</a></li>
								<li><a href="#">about us</a></li>
								<li><a href="#">gallery</a></li>
								<li><a href="#">contact</a></li>
							</ul>
						</div> -->
						
						<!--Copyright Column-->
						<!-- <div class="copyright-column col-lg-6 col-md-12 col-sm-12">
							<div class="copyright"><a target="_blank" href="https://www.templateshub.net">Templates Hub</a></div>
						</div> -->
<!-- 						
					</div>
				</div>
			</div>
		</div> -->
		
	</footer>
	
</div>
<!--End pagewrapper-->

<!-- SCROLL BACK BUTTON AND PROGRESS BAR  -->
<a class="topp-link hidee" href="" id="js-top">
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
	<span class="screen-reader-text">Back to top</span>
  </a>

<script>
	// Set a variable for our button element.
	const scrollToTopButton = document.getElementById('js-top');
	const scrollFunc = () => {
	// Get the current scroll value
	let y = window.scrollY;
	
	// If the scroll value is greater than the window height, let's add a class to the scroll-to-top button to show it!
	if (y > 0) {
		scrollToTopButton.className = "topp-link showw";
	} else {
		scrollToTopButton.className = "topp-link hidee";
	}
	};

	window.addEventListener("scroll", scrollFunc);
	const scrollToTop = () => {
	// Let's set a variable for the number of pixels we are from the top of the document.
	const c = document.documentElement.scrollTop || document.body.scrollTop;
	
	// If that number is greater than 0, we'll scroll back to 0, or the top of the document.
	// We'll also animate that scroll with requestAnimationFrame:
	// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame
	if (c > 0) {
		window.requestAnimationFrame(scrollToTop);
		// ScrollTo takes an x and a y coordinate.
		// Increase the '10' value to get a smoother/slower scroll!
		window.scrollTo(0, c - c / 10);
	}
	};
	// When the button is clicked, run our ScrolltoTop function above!
	scrollToTopButton.onclick = function(e) {
	e.preventDefault();
	scrollToTop();
	}
	//END OF SCROLL BACK BUTTON 

	//progressBarScript
	window.onscroll = function() {myFunc()};

	function myFunc() {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
	var scrolled = (winScroll / height) * 100;
	document.getElementById("myBar").style.width = scrolled + "%";
	}
	</script>
	<!-- END OF SCROLL BACK BUTTON AND PROGRESS BAR  -->

<!-- PRELOADER SCRIPT  -->
<script>
	$(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#reloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(350).css({'overflow':'visible'});
})
	</script>
<!-- END OF PRELOADER SCRIPT  -->
<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/appear.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/jquery-ui.js"></script>
<!--Google Map APi Key-->
<!-- <script src="http://maps.google.com/maps/api/js?key=AIzaSyBg0VrLjLvDLSQdS7hw6OfZJmvHhtEV_sE"></script>
<script src="js/map-script.js"></script> -->
<!--End Google Map APi-->
<script src="js/script.js"></script>

</body>

<!--  51:42  -->
</html>